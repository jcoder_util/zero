package com.gitee.zero.commons.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * <p>
 * Description: 统一异常处理
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/5 7:27 下午
 * @see com.gitee.zero.business.exception
 */
@Getter
public class BadRequestException extends RuntimeException{

    private Integer status = BAD_REQUEST.value();

    public BadRequestException(String msg){
        super(msg);
    }

    public BadRequestException(HttpStatus status, String msg){
        super(msg);
        this.status = status.value();
    }
}

