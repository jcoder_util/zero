package com.gitee.zero.commons.core.exception.handler;

import com.gitee.zero.commons.core.exception.ValidateCodeException;
import com.gitee.zero.commons.utils.ThrowableUtils;
import com.gitee.zero.commons.dto.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <p>
 * Description: 全局异常处理器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/16 2:32 下午
 * @see com.gitee.zero.commons.core.exception.handler
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 处理所有不可知的异常
     */
    @ExceptionHandler(Throwable.class)
    public ServerResponse handleException(Throwable e){
        // 打印堆栈信息
        log.error(ThrowableUtils.getStackTrace(e));
        return ServerResponse.createByErrorMessage(e.getMessage());
    }

    /**
     * 处理验证码异常
     */
    @ExceptionHandler(value = ValidateCodeException.class)
    public ServerResponse badRequestException(ValidateCodeException e) {
        // 打印堆栈信息
        log.error(ThrowableUtils.getStackTrace(e));
        return ServerResponse.createByErrorMessage(e.getMessage());
    }

//    /**
//     * BadCredentialsException
//     */
//    @ExceptionHandler(BadCredentialsException.class)
//    public ResponseEntity<ApiError> badCredentialsException(BadCredentialsException e){
//        // 打印堆栈信息
//        String message = "坏的凭证".equals(e.getMessage()) ? "用户名或密码不正确" : e.getMessage();
//        log.error(message);
//        return buildResponseEntity(ApiError.error(message));
//    }
//
//    /**
//     * 处理自定义异常
//     */
//    @ExceptionHandler(value = BadRequestException.class)
//    public ResponseEntity<ApiError> badRequestException(BadRequestException e) {
//        // 打印堆栈信息
//        log.error(ThrowableUtil.getStackTrace(e));
//        return buildResponseEntity(ApiError.error(e.getStatus(),e.getMessage()));
//    }
//
//    /**
//     * 处理 EntityExist
//     */
//    @ExceptionHandler(value = EntityExistException.class)
//    public ResponseEntity<ApiError> entityExistException(EntityExistException e) {
//        // 打印堆栈信息
//        log.error(ThrowableUtil.getStackTrace(e));
//        return buildResponseEntity(ApiError.error(e.getMessage()));
//    }
//
//    /**
//     * 处理 EntityNotFound
//     */
//    @ExceptionHandler(value = EntityNotFoundException.class)
//    public ResponseEntity<ApiError> entityNotFoundException(EntityNotFoundException e) {
//        // 打印堆栈信息
//        log.error(ThrowableUtil.getStackTrace(e));
//        return buildResponseEntity(ApiError.error(NOT_FOUND.value(),e.getMessage()));
//    }
//
//    /**
//     * 处理所有接口数据验证异常
//     */
//    @ExceptionHandler(MethodArgumentNotValidException.class)
//    public ResponseEntity<ApiError> handleMethodArgumentNotValidException(MethodArgumentNotValidException e){
//        // 打印堆栈信息
//        log.error(ThrowableUtil.getStackTrace(e));
//        String[] str = Objects.requireNonNull(e.getBindingResult().getAllErrors().get(0).getCodes())[1].split("\\.");
//        String message = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
//        String msg = "不能为空";
//        if(msg.equals(message)){
//            message = str[1] + ":" + message;
//        }
//        return buildResponseEntity(ApiError.error(message));
//    }
//
//    /**
//     * 统一返回
//     */
//    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
//        return new ResponseEntity<>(apiError, HttpStatus.valueOf(apiError.getStatus()));
//    }
}
