package com.gitee.zero.commons.core.exception;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/16 2:52 下午
 * @see com.gitee.zero.commons.core.exception
 */
public class ValidateCodeException extends RuntimeException {
    private static final long serialVersionUID = -7285211528095468156L;

    public ValidateCodeException() {
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
