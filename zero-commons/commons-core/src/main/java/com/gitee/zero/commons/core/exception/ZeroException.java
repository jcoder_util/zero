package com.gitee.zero.commons.core.exception;

import com.gitee.zero.commons.dto.ResultCodeEnum;
import lombok.Data;

/**
 * <p>
 * Description: 自定义全局异常类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/6/16 11:21
 * @see com.gitee.zero.commons.core.exception
 */
@Data
public class ZeroException extends RuntimeException {
    private Integer code;

    public ZeroException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ZeroException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "CMSException{" + "code=" + code + ", message=" + this.getMessage() + '}';
    }
}

