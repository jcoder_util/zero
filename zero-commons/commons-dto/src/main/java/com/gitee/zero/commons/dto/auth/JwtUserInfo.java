package com.gitee.zero.commons.dto.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Collection;
import java.util.Date;

/**
 * <p>
 * Description: JWT封装用户类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/13 6:52 下午
 * @see com.gitee.zero.business.controller
 */
@Data
@AllArgsConstructor
@ToString
public class JwtUserInfo {

    private  Long id;

    private  String username;

    private  String nickName;

    private  String sex;

    @JsonIgnore
    private  String password;

    private  String avatar;

    private  String email;

    private  String phone;

    private  String dept;

    private  String job;

    private  Collection<String> roles;

    private  boolean enabled;

    private Date createTime;

    public JwtUserInfo() {
    }
}
