package com.gitee.zero.commons.dto;

/**
 * @author 张传臣
 *
 */
/**
 * <p>
 * Description: 返回的code
 * 一般认为非200,就是错误。
 * 一个接口返回的code除了200和500之外，会有其他的值，但种类不会太多。
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/16 2:32 下午
 * @see com.gitee.zero.commons.utils.dto
 */
public enum ResponseCode {

    /**
     * 查询成功
     */
    SUCCESS(200,"SUCCESS"),

    /**
     * 查询失败
     */
    ERROR(500,"ERROR"),

    /**
     * 熔断请求
     */
    BREAKING(20004,"请求失败了，请检查您的网络")
    ;

    private final int code;
    private final String desc;


    ResponseCode(int code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }
    public String getDesc(){
        return desc;
    }

}
