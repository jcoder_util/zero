package com.gitee.zero.commons.dto;

import lombok.Getter;

/**
 * <p>
 * Description: 通用数据传输对象状态枚举类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/13 4:42 下午
 * @see com.gitee.zero.commons.utils.dto
 */
@Getter
public enum  ResultCodeEnum {

    /**
     * 请求成功
     */
    SUCCESS(true,20000,"请求成功"),
    /**
     * 未知错误
     */
    UNKNOWN_ERROR(false,20001,"未知错误"),
    /**
     * 参数错误
     */
    PARAM_ERROR(false,20002,"参数错误"),
    /**
     * 空指针异常
     */
    NULL_POINT(false,20003,"空指针异常"),
    /**
     * 客户端错误
     */
    HTTP_CLIENT_ERROR(false,20004,"客户端错误")
    ;

    // 响应是否成功
    private Boolean success;
    // 响应状态码
    private Integer code;
    // 响应信息
    private String message;

    ResultCodeEnum(boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }
}
