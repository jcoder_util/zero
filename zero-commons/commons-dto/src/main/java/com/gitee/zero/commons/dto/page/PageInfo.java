package com.gitee.zero.commons.dto.page;

import lombok.Data;

import java.util.List;

/**
 * <p>
 * Description: 前台分页结果封装
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/8 10:59 上午
 * @see com.gitee.zero.commons.dto.page
 */
@Data
public class PageInfo {

    private List content;

    private long total;
}
