package com.gitee.zero.commons.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * Description: 树形视图节点类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/27 11:35 下午
 * @see com.gitee.zero.business.exception
 */
@Data
public class TreeViewNode {
    private Long id;

    private String name;

    @NotNull
    private Long enabled;

    private Long pid;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeViewNode> children;

    private Date createTime;

    public String getLabel() {
        return name;
    }
}
