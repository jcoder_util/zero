package com.gitee.zero.commons.log.event;

import com.gitee.zero.provider.domain.Log;
import org.springframework.context.ApplicationEvent;

/**
 * <p>
 * Description: 系统日志事件
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 7:15 下午
 * @see com.gitee.zero.commons.log.event
 */
public class LogEvent extends ApplicationEvent {

    public LogEvent(Log source) {
        super(source);
    }
}
