package com.gitee.zero.commons.log.event;

import com.gitee.zero.business.feign.LogFeign;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.provider.domain.Log;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * <p>
 * Description: 异步监听日志事件
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 9:55 下午
 * @see com.gitee.zero.commons.log.event
 */
@AllArgsConstructor
public class LogListener {

    private final LogFeign logFeign;

    @Async
    @Order
    @EventListener(LogEvent.class)
    public void saveLog(LogEvent event) {
        Log log = (Log) event.getSource();
        ServerResponse serverResponse = logFeign.create(log);
        System.out.println(serverResponse.getMessage());
    }
}
