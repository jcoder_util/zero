package com.gitee.zero.commons.log.init;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * <p>
 * Description: 通过环境变量的形式注入 logging.file
 *              自动维护 Spring Boot Admin Logger Viewer
 *              这个类不太懂啊，先抄过来看看什么意思
 * </p>
 * @author lengleng
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 10:30 下午
 * @see com.gitee.zero.commons.log.init
 */
public class ApplicationLoggerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        ConfigurableEnvironment environment = applicationContext.getEnvironment();

        String appName = environment.getProperty("spring.application.name");

        String logBase = environment.getProperty("LOGGING_PATH", "logs");
        // spring boot admin 直接加载日志
        System.setProperty("logging.file", String.format("%s/%s/debug.log", logBase, appName));
    }
}
