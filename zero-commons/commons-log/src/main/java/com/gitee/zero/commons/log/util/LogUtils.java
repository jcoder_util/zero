package com.gitee.zero.commons.log.util;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.gitee.zero.commons.security.util.SecurityUtils;
import com.gitee.zero.provider.domain.Log;
import lombok.experimental.UtilityClass;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * <p>
 * Description: 系统日志工具类
 * </p>
 *
 * @author L.cm
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 12:08 下午
 * @see com.gitee.zero.commons.log.util
 */
@UtilityClass
public class LogUtils {
    public Log getLog() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        Log log = new Log();
        log.setUsername(SecurityUtils.getUserName());
        log.setLogType("INFO");
        log.setRequestIp(ServletUtil.getClientIP(request));
        log.setMethod(request.getMethod());
        log.setAddress(URLUtil.getPath(request.getRequestURI()));
        log.setParams(HttpUtil.toParams(request.getParameterMap()));
        return log;
    }
}
