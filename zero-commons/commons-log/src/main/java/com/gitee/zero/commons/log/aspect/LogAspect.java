package com.gitee.zero.commons.log.aspect;

import com.gitee.zero.commons.log.annotation.Log;
import com.gitee.zero.commons.log.event.LogEvent;
import com.gitee.zero.commons.log.util.LogUtils;
import com.gitee.zero.commons.security.util.SpringContextHolder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;

/**
 * <p>
 * Description: 操作日志使用spring event异步入库
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/7 6:03 下午
 * @see com.gitee.zero.commons.log.aspect
 */
@Aspect
@Slf4j
@DependsOn("springContextHolder")
public class LogAspect {
    @Autowired
    private ApplicationContext applicationContext;

    @Around("@annotation(around)")
    @SneakyThrows
    public Object around(ProceedingJoinPoint point, Log around) {
        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        log.debug("[类名]:{},[方法]:{}", strClassName, strMethodName);

        com.gitee.zero.provider.domain.Log log = LogUtils.getLog();
        log.setDescription(around.value());
        Long startTime = System.currentTimeMillis();
        // 放行
        Object obj = point.proceed();
        //执行目标方法之后的操作
        Long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);
        // 发送异步日志事件
        applicationContext.publishEvent(new LogEvent(log));
//        SpringContextHolder.publishEvent(new LogEvent(log));
        //将目标方法执行的结果返回
        return obj;
    }
}
