package com.gitee.zero.commons.log.config;

import com.gitee.zero.business.feign.LogFeign;
import com.gitee.zero.commons.log.aspect.LogAspect;
import com.gitee.zero.commons.log.event.LogListener;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * <p>
 * Description: 日志自动配置
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 10:09 下午
 * @see com.gitee.zero.commons.log.config
 */
@EnableAsync
@Configuration
@AllArgsConstructor
@ConditionalOnWebApplication
public class LogAutoConfiguration {

    private final LogFeign logFeign;

    @Bean
    public LogListener logListener() {
        return new LogListener(logFeign);
    }

    @Bean
    public LogAspect logAspect() {
        return new LogAspect();
    }
}
