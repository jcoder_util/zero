package com.gitee.zero.commons.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Description: 操作日志注解
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/7 6:00 下午
 * @see com.gitee.zero.commons.log.annotation
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /**
     * 描述
     *
     * @return {String}
     */
    String value();
}
