package com.gitee.zero.commons.utils;

import javax.validation.ConstraintViolationException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/16 2:36 下午
 * @see com.gitee.zero.commons.utils
 */
public class ThrowableUtils {
    /**
     * 获取堆栈信息
     */
    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }

//    public static void throwForeignKeyException(Throwable e, String msg){
//        Throwable t = e.getCause();
//        while ((t != null) && !(t instanceof ConstraintViolationException)) {
//            t = t.getCause();
//        }
//        if (t != null) {
//            throw new BadRequestException(msg);
//        }
//        assert false;
//        throw new BadRequestException("删除失败：" + t.getMessage());
//    }
}
