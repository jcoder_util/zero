package com.gitee.zero.commons.security.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description: Security工具类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/21 3:44 下午
 * @see com.gitee.zero.commons.security.util
 */
@Component
@DependsOn("springContextHolder")
public class SecurityUtils123 {

    private TokenStore tokenStore = SpringContextHolder.getBean(TokenStore.class);

    public static SecurityUtils123 me() {
        return SpringContextHolder.getBean("securityUtils");
    }

    public String getUserName(String authHeader){
        String tokenValue = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        OAuth2Authentication auth2Authentication = tokenStore.readAuthentication(accessToken);
        return auth2Authentication.getPrincipal().toString();
    }
}
