package com.gitee.zero.commons.security.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Description: 自定义Token异常信息, 用于tokan校验失败返回信息, 比如token过期/验证错误
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/2 5:10 下午
 * @see com.gitee.zero.business.exception
 */
@Component
public class ZeroAuthExceptionEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws ServletException {
        ResultEnum resultEnum = ResultEnum.CODE_401;
        Map map = new HashMap(4);
        map.put("status", resultEnum.getCode());
        map.put("msg", resultEnum.getMsg());
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(response.getOutputStream(), map);
        } catch (Exception e) {
            throw new ServletException();
        }
    }
}