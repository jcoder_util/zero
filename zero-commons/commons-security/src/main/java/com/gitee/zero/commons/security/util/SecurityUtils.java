package com.gitee.zero.commons.security.util;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/22 4:22 下午
 * @see com.gitee.zero.commons.security.util
 */
public class SecurityUtils {
    public static String getUserName(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
