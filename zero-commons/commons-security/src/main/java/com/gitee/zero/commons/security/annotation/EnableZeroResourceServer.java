package com.gitee.zero.commons.security.annotation;

import com.gitee.zero.commons.security.component.PermissionService;
import com.gitee.zero.commons.security.component.ZeroSecurityBeanDefinitionRegistrar;
import com.gitee.zero.commons.security.util.SpringContextHolder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Description: 资源服务注解
 * </p>
 *
 * @author lengleng
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/3 9:33 上午
 * @see com.gitee.zero.commons.security.annotation
 */
@Documented
@Inherited
@EnableResourceServer
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@Import({ZeroSecurityBeanDefinitionRegistrar.class, PermissionService.class, SpringContextHolder.class})
public @interface EnableZeroResourceServer {
}
