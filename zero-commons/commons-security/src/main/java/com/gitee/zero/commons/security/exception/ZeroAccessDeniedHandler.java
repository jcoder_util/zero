package com.gitee.zero.commons.security.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Description: 授权失败处理异常
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/27 11:35 下午
 * @see com.gitee.zero.business.exception
 */
@Component
public class ZeroAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ObjectMapper objectMapper = new ObjectMapper();
        response.setContentType("application/json;charset=UTF-8");
        ResultEnum resultEnum = ResultEnum.CODE_403;
        Map map = new HashMap(4);
        map.put("status", resultEnum.getCode());
        map.put("msg", resultEnum.getMsg());
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(objectMapper.writeValueAsString(map));
    }
}