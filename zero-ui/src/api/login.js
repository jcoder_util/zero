import request from '@/utils/request'

export function login(username, password, code, uuid, grant_type) {
  return request({
    url: 'oauth/token',
    method: 'post',
    headers: {
      'Authorization': 'Basic Y2xpZW50OnNlY3JldA==',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    params: {
      username,
      password,
      code,
      uuid,
      grant_type
    }
  })
}

export function getInfo() {
  return request({
    url: 'oauth/info',
    method: 'get'
  })
}

export function getCodeImg() {
  return request({
    url: 'oauth/code',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: 'oauth/logout',
    method: 'delete'
  })
}
