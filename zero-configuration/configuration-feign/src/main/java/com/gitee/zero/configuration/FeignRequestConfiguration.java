package com.gitee.zero.configuration;

import com.gitee.zero.interceptor.FeignRequestInterceptor;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

/**
 * <p>
 * Description: Feign 全局配置
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/2 11:16 上午
 * @see com.gitee.zero.configuration
 */
@Configuration
@ConditionalOnWebApplication
public class FeignRequestConfiguration {
    /**
     * jwt 拦截器
     * @return
     */
    @Bean
    public RequestInterceptor requestInterceptor() {
        return new FeignRequestInterceptor();
    }

    /**
     * feign 日志
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }

    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }

}
