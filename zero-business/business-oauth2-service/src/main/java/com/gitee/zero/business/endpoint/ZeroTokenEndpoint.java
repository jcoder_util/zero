package com.gitee.zero.business.endpoint;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gitee.zero.commons.core.constant.CommonConstants;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.auth.JwtUserInfo;
import com.gitee.zero.commons.utils.MapperUtils;
import com.wf.captcha.ArithmeticCaptcha;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Description: 重写令牌申请接口
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/12 7:12 下午
 * @see com.gitee.zero.business.endpoint
 */
@RestController
@RequestMapping("/oauth")
@Slf4j
public class ZeroTokenEndpoint {

    private final TokenEndpoint tokenEndpoint;

    private final CheckTokenEndpoint checkTokenEndpoint;

    private final TokenStore tokenStore;

    private final RedisTemplate<String, String> redisTemplate;

    private static final String USER = "user";

    public ZeroTokenEndpoint(TokenEndpoint tokenEndpoint, CheckTokenEndpoint checkTokenEndpoint, TokenStore tokenStore, RedisTemplate<String, String> redisTemplate) {
        this.tokenEndpoint = tokenEndpoint;
        this.checkTokenEndpoint = checkTokenEndpoint;
        this.tokenStore = tokenStore;
        this.redisTemplate = redisTemplate;
    }

    @GetMapping("/token")
    public ServerResponse getAccessToken(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        return generateAccessToken(tokenEndpoint.getAccessToken(principal, parameters).getBody());
    }

    @PostMapping("/token")
    public ServerResponse postAccessToken(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        log.info(parameters.toString());
        return generateAccessToken(tokenEndpoint.postAccessToken(principal, parameters).getBody());
    }

    /**
     * 自定义返回格式
     *
     * @param accessToken
     * @return
     */
    private ServerResponse generateAccessToken(OAuth2AccessToken accessToken) {
        DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
        Map<String, Object> data = new LinkedHashMap(token.getAdditionalInformation());
        data.put("token", "Bearer " + token.getValue());
        if (token.getRefreshToken() != null) {
            data.put("refreshToken", token.getRefreshToken().getValue());
        }
        data.remove("refreshToken");
        data.remove("jti");
        return ServerResponse.createBySuccess("获取token成功", data);
    }

    /**
     * 获取用户信息
     *
     * @return
     */
    @SneakyThrows
    @GetMapping(value = "/info")
    public ServerResponse<JwtUserInfo> getUserInfo(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader) {
        if (StrUtil.isBlank(authHeader)) {
            return ServerResponse.createByErrorMessage("获取用户信息失败，token 为空");
        }
        String tokenValue = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        Map<String, Object> response = (Map<String, Object>) checkTokenEndpoint.checkToken(accessToken.getValue());
        if (ObjectUtil.isNull(response.get(USER))) {
            return ServerResponse.createByErrorMessage("获取用户信息失败，token 无效");
        }
        JwtUserInfo jwtUserInfo = MapperUtils.map2pojo((Map) response.get(USER), JwtUserInfo.class);
        return ServerResponse.createBySuccess("获取用户信息成功", jwtUserInfo);
    }

    @GetMapping(value = "/code")
    public ServerResponse<Object> getCode() {
        // 算术类型 https://gitee.com/whvse/EasyCaptcha
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(111, 36);
        // 几位数运算，默认是两位
        captcha.setLen(2);
        // 获取运算的结果
        String result = captcha.text();
        String uuid = IdUtil.simpleUUID();
        // 保存
        redisTemplate.opsForValue().set(CommonConstants.DEFAULT_CODE_KEY + uuid, result, 300, TimeUnit.SECONDS);

        // 验证码信息
        Map<String, Object> imgResult = new HashMap<String, Object>(2) {{
            put("img", captcha.toBase64());
            put("uuid", uuid);
        }};
        return ServerResponse.createBySuccess("获取验证码成功", imgResult);
    }

    /**
     * 退出并删除token
     *
     * @param authHeader
     * @return
     */
    @DeleteMapping(value = "/logout")
    public ServerResponse<Object> logout(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader) {
        if (StrUtil.isBlank(authHeader)) {
            return ServerResponse.createByErrorMessage("退出失败，token 为空");
        }
        String tokenValue = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        if (accessToken == null || StrUtil.isBlank(accessToken.getValue())) {
            return ServerResponse.createByErrorMessage("退出失败，token 无效");
        }

//        OAuth2Authentication auth2Authentication = tokenStore.readAuthentication(accessToken);
//        // 清空用户信息
//
//        // 清空access token
//        tokenStore.removeAccessToken(accessToken);
//
//        // 清空 refresh token
//        OAuth2RefreshToken refreshToken = accessToken.getRefreshToken();
//        tokenStore.removeRefreshToken(refreshToken);
        return ServerResponse.createBySuccess("退出并删除token成功");
    }

}
