package com.gitee.zero.business.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;


/**
 * <p>
 * Description: 认证服务器配置
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.config
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    /**
     * 密码加密
     */
    private final BCryptPasswordEncoder passwordEncoder;

    /**
     * 用于支持 password 模式
     */
    private final AuthenticationManager authenticationManager;

    /**
     * 用于支持 授权码模式 模式
     */
    @Autowired
    private AuthorizationCodeServices authorizationCodeServices;

    public AuthorizationServerConfiguration(BCryptPasswordEncoder passwordEncoder, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 对Jwt签名时，增加一个密钥
     * JwtAccessTokenConverter：对Jwt来进行编码以及解码的类
     *
     * 非对称加密方式(jks文件可能过期，jks文件需要Java keytool工具生成)
     * keytool -genkey -alias jwt -keyalg  RSA -keysize 1024 -validity 365 -keystore jwt.jks
     * 使用keytool生成密钥，别名为jwt，算法为RSA，有效期为365天，文件名为jwt.jks,需要输入密码,"123456"
     *
     * KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource("zero.jks"), "123456".toCharArray());
     * jwtAccessTokenConverter.setKeyPair(keyStoreKeyFactory.getKeyPair("zero"));
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        //对称加密方式
        jwtAccessTokenConverter.setSigningKey("zero");
        return jwtAccessTokenConverter;
    }

    /**
     * 注入自定义token生成方式（jwt）
     *
     * @return
     */
    @Bean
    public TokenEnhancer zeroTokenEnhancer() {
        return new ZeroTokenEnhancer();
    }

    /**
     * token可以保存在内存中，也可以保存在数据库、Redis中。
     * 如果保存在数据库、Redis，那么资源服务器与认证服务器可以不在同一个工程中。
     * 如果不保存access_token，则没法通过access_token取得用户信息。
     * return new InMemoryTokenStore();//存内存
     * RedisConnectionFactory redisConnectionFactory
     * return new RedisTokenStore(redisConnectionFactory);//存redis
     * return new JdbcTokenStore(dataSource);//存数据库
     * @return
     */
    @Bean
    public TokenStore tokenStore() {
        //jwt存储
        return new JwtTokenStore(jwtAccessTokenConverter());
    }


//    @Bean
//    public ClientDetailsService jdbcClientDetailsService() {
//        // 基于 JDBC 实现，需要事先在数据库配置客户端信息
//        return new JdbcClientDetailsService(dataSource());
//    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        // 自定义jwt生成token方式
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(zeroTokenEnhancer(), jwtAccessTokenConverter()));
        endpoints
                // 用于支持密码模式
                .authenticationManager(authenticationManager)
                // 授权码模式
                .authorizationCodeServices(authorizationCodeServices)
                .tokenStore(tokenStore())
                .tokenEnhancer(tokenEnhancerChain)
                .accessTokenConverter(jwtAccessTokenConverter());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        /*
         *  /oauth/authorize : 授权端点
         *  /oauth/token : 令牌端点
         *  /oauth/confirm_access : 用户确认授权提交端点
         *  /oauth/error : 授权服务错误信息端点
         *  /oauth/check_token : 用于资源服务访问的令牌解析端点
         *  /oauth/token_key : 提供公钥的端点，如果使用的是JWT令牌。
         *
         */
        security
                // 公钥公开
                .tokenKeyAccess("permitAll()")
                // 允许客户端访问 /oauth/check_token 检查 token
                .checkTokenAccess("isAuthenticated()")
                // 表单认证
                .allowFormAuthenticationForClients();
    }

    /**
     * 配置客户端
     *
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // 客户端配置
        clients.inMemory()
                .withClient("client")
                .secret(passwordEncoder.encode("secret"))
                .resourceIds("admin")
                .authorizedGrantTypes("authorization_code", "client_credentials", "password", "refresh_token")
                .scopes("app")
                .redirectUris("http://localhost:8080")
                .accessTokenValiditySeconds(3 * 60 * 60)
                .refreshTokenValiditySeconds(3 * 60 * 60);
        //clients.withClientDetails(jdbcClientDetailsService());
    }

    /**
     * 授权码的使用模式
     *
     * @return
     */
    @Bean
    @Primary
    public AuthorizationCodeServices authorizationCodeServices() {
        return new InMemoryAuthorizationCodeServices();
        //return new JdbcAuthorizationCodeServices(dataSource);
    }
}
