package com.gitee.zero.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>
 * Description: Auth服务启动类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/8 11:02 上午
 * @see
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
public class BusinessOauth2ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BusinessOauth2ServiceApplication.class, args);
    }
}
