package com.gitee.zero.business.config;

import com.gitee.zero.commons.dto.auth.JwtUser;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Description: 自定义token生成携带的信息
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.config
 */
public class ZeroTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>(4);
        JwtUser jwtUser = (JwtUser) oAuth2Authentication.getUserAuthentication().getPrincipal();
        additionalInfo.put("user", jwtUser);
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionalInfo);
        return oAuth2AccessToken;
    }
}
