package com.gitee.zero.business.service;


import com.gitee.zero.commons.dto.auth.JwtUser;
import com.gitee.zero.provider.api.MenuService;
import com.gitee.zero.provider.api.RolesMenusService;
import com.gitee.zero.provider.api.UserService;
import com.gitee.zero.provider.domain.Menu;
import com.gitee.zero.provider.domain.RolesMenus;
import com.gitee.zero.provider.domain.User;
import com.google.common.collect.Lists;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Description: 自定义用户认证实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/8 11:02 上午
 * @see
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Reference(version = "1.0.0")
    private UserService userService;

    @Reference(version = "1.0.0")
    private RolesMenusService rolesMenusService;

    @Reference(version = "1.0.0")
    private MenuService menuService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // 查询用户
        User user = userService.get(userName);
        // 用户存在
        if (user != null) {
            return createJwtUser(user);
        }
        // 用户不存在
        else {
            return null;
        }
    }

    private JwtUser createJwtUser(User user) {

        List<GrantedAuthority> grantedAuthorities = Lists.newArrayList();

        List<RolesMenus> rolesMenusList = rolesMenusService.getMenusByRole(user.getId());

        List<Long> menuIds = new ArrayList<>();
        rolesMenusList.forEach(rolesMenus -> {
            menuIds.add(rolesMenus.getMenuId());
        });
        List<Menu> menuList = menuService.getAllMenus(menuIds);
        menuList.forEach(menu -> {
            if (!StringUtils.isEmpty(menu.getPermission())) {
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(menu.getPermission());
                grantedAuthorities.add(grantedAuthority);
            }
        });

        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getNickName(),
                user.getSex(),
                user.getPassword(),
                user.getAvatar(),
                user.getEmail(),
                user.getPhone(),
                user.getDeptId().toString(),
                user.getJobId().toString(),
                grantedAuthorities,
                user.getEnabled(),
                user.getCreateTime(),
                user.getLastPasswordResetTime()
        );
    }
}
