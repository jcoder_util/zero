package com.gitee.zero.business.feign;

import com.gitee.zero.business.feign.fallback.LogFeignFallback;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.configuration.FeignRequestConfiguration;
import com.gitee.zero.provider.domain.Log;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>
 * Description: 系统日志
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 11:55 上午
 * @see com.gitee.zero.business.feign
 */
@Component
@FeignClient(value = "business-admin", configuration = FeignRequestConfiguration.class, fallback = LogFeignFallback.class)
public interface LogFeign {

    /**
     * 创建日志
     * @param log
     * @return
     */
    @PostMapping("/api/log")
    ServerResponse create(@RequestBody Log log);

}