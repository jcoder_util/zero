package com.gitee.zero.business.feign.fallback;

import com.gitee.zero.business.feign.LogFeign;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.provider.domain.Log;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description: 系统日志 熔断器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/9 11:58 上午
 * @see com.gitee.zero.business.feign.fallback
 */
@Component
public class LogFeignFallback implements LogFeign {
    @Override
    public ServerResponse create(Log log) {
        return ServerResponse.createByErrorMessage("提交日志信息失败");
    }
}
