package com.gitee.zero.business.feign;

import com.gitee.zero.business.feign.fallback.OauthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * <p>
 * Description: feign调用oauth2服务
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.feign.fallback
 */
@Component
@FeignClient(value = "business-oauth2", fallback = OauthFeignFallback.class)
public interface Oauth2Feign {
    /**
     * 获取token
     *
     * @param username      {@code String} 用户名
     * @param password      {@code String} 密码
     * @param client_id     {@code String} 客户端ID
     * @param client_secret {@code String} 客户端密钥
     * @param grant_type    {@code String} 授权类型
     * @param scope         {@code String} 授权范围
     * @return {@code String} JSON
     */
    @PostMapping(value = "/oauth/token", headers = {"Content-Type: multipart/form-data"})
    Map<String, String> getOauthToken(@RequestParam("username") String username,
                                      @RequestParam("password") String password,
                                      @RequestParam("client_id") String client_id,
                                      @RequestParam("client_secret") String client_secret,
                                      @RequestParam("grant_type") String grant_type,
                                      @RequestParam("scope") String scope);

    /**
     * 校验token
     *
     * @param token
     * @return
     */
    @GetMapping(value = "/oauth/check_token")
    String checkToken(@RequestParam("token") String token);


}
