package com.gitee.zero.business.feign.fallback;

import com.gitee.zero.business.feign.Oauth2Feign;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p>
 * Description: 服务认证熔断器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.feign.fallback
 */
@Component
public class OauthFeignFallback implements Oauth2Feign {

    public static final String BREAKING_MESSAGE = "请求失败了，请检查您的网络";

    @Override
    public Map<String, String> getOauthToken(String username, String password, String client_id, String client_secret, String grant_type, String scope) {
        return null;
    }

    @Override
    public String checkToken(String token) {
        return null;
    }

}
