package com.gitee.zero.business.controller;

import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.TreeViewNode;
import com.gitee.zero.commons.log.annotation.Log;
import com.gitee.zero.provider.api.DeptService;
import com.gitee.zero.provider.domain.Dept;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Description: 部门控制器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/dept")
public class DeptController {

    private static final String ENTITY_NAME = "dept";

    @Reference(version = "1.0.0")
    private DeptService deptService;

    @Log("获取部门树")
    @GetMapping
    public ServerResponse<Map<String, Object>> getDeptTree() {
        List<TreeViewNode> treeViewNodes = deptService.findTreeViewNode();
        Map<String, Object> map = deptService.buildTree(treeViewNodes);
        return ServerResponse.createBySuccess("获取部门树成功", map);

    }

    @PostMapping
    @PreAuthorize("@pms.hasPermission('dept:add')")
    public ServerResponse create(@Validated @RequestBody Dept resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        deptService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    @PreAuthorize("@pms.hasPermission('dept:edit')")
    public ServerResponse update(@RequestBody Dept resources){
        deptService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @DeleteMapping
    @PreAuthorize("@pms.hasPermission('dept:del')")
    public ServerResponse delete(@RequestBody Set<Long> ids){
        deptService.removeByIds(ids);
        return ServerResponse.createBySuccessMessage("删除成功");
    }
}