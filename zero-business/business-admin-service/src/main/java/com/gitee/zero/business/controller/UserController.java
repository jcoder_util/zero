package com.gitee.zero.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zero.business.factory.PageFactory;
import com.gitee.zero.cloud.feign.UploadFeign;
import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ResponseCode;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.page.PageInfo;
import com.gitee.zero.commons.log.annotation.Log;
import com.gitee.zero.commons.security.util.SecurityUtils123;
import com.gitee.zero.provider.api.UserService;
import com.gitee.zero.provider.domain.User;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

/**
 * <p>
 * Description: 系统用户控制器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/29 11:39 上午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/users")
public class UserController {

    private static final String ENTITY_NAME = "user";

    @Reference(version = "1.0.0")
    private UserService userService;

    private UploadFeign uploadFeign;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final String USER = "user";

    public UserController(UploadFeign uploadFeign, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.uploadFeign = uploadFeign;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Log("用户列表")
    @GetMapping
    @PreAuthorize("@pms.hasPermission('user:list')")
    public ServerResponse<PageInfo> getUsers(Page page) {
        IPage<User> iPage = userService.page(page);
        return ServerResponse.createBySuccess("获取用户列表成功", PageFactory.createPageInfo(iPage));
    }

    /**
     * 新增用户
     *
     * @param resources
     * @return
     */
    @Log("新增用户")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('user:add')")
    public ServerResponse create(@Validated @RequestBody User resources) {
        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        checkLevel(resources);
        // 默认密码 123456
        resources.setPassword(bCryptPasswordEncoder.encode("123456"));
        userService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    /**
     * 修改用户
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('user:edit')")
    public ServerResponse update(@Validated @RequestBody User resources) {
        checkLevel(resources);
        userService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    /**
     * 修改用户：个人中心
     *
     * @param resources
     * @return
     */
    @PutMapping(value = "center")
    public ServerResponse center(@Validated @RequestBody User resources) {
//        UserDto userDto = userService.findByName(SecurityUtils.getUsername());
//        if (!resources.getId().equals(userDto.getId())) {
//            throw new BadRequestException("不能修改他人资料");
//        }
//        userService.updateCenter(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @DeleteMapping
    @PreAuthorize("@pms.hasPermission('user:del')")
    public ServerResponse delete(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader, @RequestBody Set<Long> ids) {
        userService.removeByIds(ids);
        String userName = SecurityUtils123.me().getUserName(authHeader);
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    @PostMapping(value = "/updatePass")
//    public ServerResponse updatePass(@RequestBody UserPassVo passVo) {
    public ServerResponse updatePass() {

        // 密码解密
//        RSA rsa = new RSA(privateKey, null);
//        String oldPass = new String(rsa.decrypt(passVo.getOldPass(), KeyType.PrivateKey));
//        String newPass = new String(rsa.decrypt(passVo.getNewPass(), KeyType.PrivateKey));
//        UserDto user = userService.findByName(SecurityUtils.getUsername());
//        if (!passwordEncoder.matches(oldPass, user.getPassword())) {
//            throw new BadRequestException("修改失败，旧密码错误");
//        }
//        if (passwordEncoder.matches(newPass, user.getPassword())) {
//            throw new BadRequestException("新密码不能与旧密码相同");
//        }
//        userService.updatePass(user.getUsername(), passwordEncoder.encode(newPass));
        return ServerResponse.createBySuccessMessage("修改密码成功");
    }

    @PostMapping(value = "/updateAvatar")
    public ServerResponse updateAvatar(@RequestParam MultipartFile file) {
        ServerResponse serverResponse = uploadFeign.upload(file);
        if (serverResponse.getStatus() == ResponseCode.SUCCESS.getCode()) {

        }
        return serverResponse;
    }

    /**
     * 修改邮箱
     *
     * @param code
     * @param user
     * @return
     */
    @PostMapping(value = "/updateEmail/{code}")
    public ServerResponse updateEmail(@PathVariable String code, @RequestBody User user) {
        // 密码解密
//        RSA rsa = new RSA(privateKey, null);
//        String password = new String(rsa.decrypt(user.getPassword(), KeyType.PrivateKey));
//        UserDto userDto = userService.findByName(SecurityUtils.getUsername());
//        if (!passwordEncoder.matches(password, userDto.getPassword())) {
//            throw new BadRequestException("密码错误");
//        }
//        VerificationCode verificationCode = new VerificationCode(code, ElAdminConstant.RESET_MAIL, "email", user.getEmail());
//        verificationCodeService.validated(verificationCode);
//        userService.updateEmail(userDto.getUsername(), user.getEmail());
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    /**
     * 如果当前用户的角色级别低于创建用户的角色级别，则抛出权限不足的错误
     *
     * @param resources /
     */
    private void checkLevel(User resources) {
//        UserDto user = userService.findByName(SecurityUtils.getUsername());
//        Integer currentLevel = Collections.min(roleService.findByUsersId(user.getId()).stream().map(RoleSmallDto::getLevel).collect(Collectors.toList()));
//        Integer optLevel = roleService.findByRoles(resources.getRoles());
//        if (currentLevel > optLevel) {
//            throw new BadRequestException("角色权限不足");
//        }
    }

}
