package com.gitee.zero.business.controller;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zero.business.factory.PageFactory;
import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.page.PageInfo;
import com.gitee.zero.commons.log.annotation.Log;
import com.gitee.zero.provider.api.MenuService;
import com.gitee.zero.provider.api.RoleService;
import com.gitee.zero.provider.api.RolesMenusService;
import com.gitee.zero.provider.domain.Role;
import com.gitee.zero.provider.domain.RolesMenus;
import com.gitee.zero.provider.dto.RoleDto;
import com.gitee.zero.provider.dto.RoleMenuDto;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/29 6:37 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private static final String ENTITY_NAME = "role";

    @Reference(version = "1.0.0")
    private RoleService roleService;

    @Reference(version = "1.0.0")
    private RolesMenusService rolesMenusService;

    @Reference(version = "1.0.0")
    private MenuService menuService;

    @GetMapping(value = "/{id}")
    @PreAuthorize("@pms.hasPermission('roles:list')")
    public ServerResponse<RoleDto> getRoles(@PathVariable Long id){
        Role role = roleService.getById(id);
        RoleDto roleDto = roleService.getRoleDto(role);
        return ServerResponse.createBySuccess("获取单个role成功", roleDto);
    }

    @GetMapping(value = "/all")
    public ServerResponse<Object> getAll() {
        List<RoleDto> roleDtoList = roleService.listRoleDto();
        return ServerResponse.createBySuccess("获取成功", roleDtoList);
    }

    @GetMapping
    public ServerResponse<PageInfo> getRoles(Page page) {
        IPage<RoleDto> iPage = roleService.pageRoleDto(page);
        return ServerResponse.createBySuccess("获取角色列表成功", PageFactory.createPageInfo(iPage));
    }

    @GetMapping(value = "/level")
    public ServerResponse<Object> getLevel() {
        return ServerResponse.createBySuccess("获取级别成功", Dict.create().set("level", 1));
    }

    @Log("新增角色")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('roles:add')")
    public ServerResponse create(@Validated @RequestBody Role resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
//        getLevels(resources.getLevel());
        roleService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    @PreAuthorize("@pms.hasPermission('roles:edit')")
    public ServerResponse update(@Validated @RequestBody Role resources){
        //getLevels(resources.getLevel());
        roleService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @PutMapping(value = "/menu")
    @PreAuthorize("@pms.hasPermission('roles:edit')")
    public ServerResponse updateMenu(@RequestBody RoleMenuDto roleMenuDto){
        if(ObjectUtil.hasEmpty(roleMenuDto, roleMenuDto.getRoleId(), roleMenuDto.getMenus())){
            throw new BadRequestException("传入信息为空");
        }
        rolesMenusService.removeMenusByRole(roleMenuDto.getRoleId());
        List<RolesMenus> rolesMenusList = new ArrayList<>();
        roleMenuDto.getMenus().forEach(menu->{
            RolesMenus rolesMenus = new RolesMenus();
            rolesMenus.setRoleId(roleMenuDto.getRoleId());
            rolesMenus.setMenuId(menu);
            rolesMenusList.add(rolesMenus);
        });
        rolesMenusService.saveBatch(rolesMenusList);
        return ServerResponse.createBySuccessMessage("分配成功");
    }

    @DeleteMapping
    @PreAuthorize("@pms.hasPermission('roles:del')")
    public ServerResponse delete(@RequestBody Set<Long> ids){
        roleService.removeByIds(ids);
        return ServerResponse.createBySuccessMessage("删除成功");
    }

    /**
     * 获取用户的角色级别
     * @return /
     */
//    private int getLevels(Integer level){
//        UserDto user = userService.findByName(SecurityUtils.getUsername());
//        List<Integer> levels = roleService.findByUsersId(user.getId()).stream().map(RoleSmallDto::getLevel).collect(Collectors.toList());
//        int min = Collections.min(levels);
//        if(level != null){
//            if(level < min){
//                throw new BadRequestException("权限不足，你的角色级别：" + min + "，低于操作的角色级别：" + level);
//            }
//        }
//        return null;
//    }
}
