package com.gitee.zero.business.controller;

import com.gitee.zero.commons.dto.ServerResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Description: 获取访问数据
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/28 1:36 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/visits")
public class VisitsController {

    @PostMapping
    public ServerResponse<Object> create(){
        return ServerResponse.createBySuccessMessage("获取访问数据成功");
    }

}
