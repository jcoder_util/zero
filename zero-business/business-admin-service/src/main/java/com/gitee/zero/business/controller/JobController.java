package com.gitee.zero.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zero.business.factory.PageFactory;
import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.page.PageInfo;
import com.gitee.zero.provider.api.JobService;
import com.gitee.zero.provider.domain.Job;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * <p>
 * Description: 岗位控制器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/28 10:11 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/job")
public class JobController {
    @Reference(version = "1.0.0")
    private JobService jobService;

    private static final String ENTITY_NAME = "job";

    @GetMapping
    @PreAuthorize("@pms.hasPermission('job:list')")
    public ServerResponse<PageInfo> getJobs(Page page){
        IPage<Job> iPage = jobService.page(page);
        return ServerResponse.createBySuccess("获取岗位列表成功", PageFactory.createPageInfo(iPage));
    }

    @PostMapping
    public ServerResponse create(@Validated @RequestBody Job resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        jobService.save(resources);
        System.out.println(resources.toString());
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    public ServerResponse update(@RequestBody Job resources){
        jobService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @DeleteMapping
    public ServerResponse delete(@RequestBody Set<Long> ids){
        jobService.removeByIds(ids);
        return ServerResponse.createBySuccessMessage("删除成功");
    }
}
