package com.gitee.zero.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zero.business.factory.PageFactory;
import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.page.PageInfo;
import com.gitee.zero.provider.api.DictDetailService;
import com.gitee.zero.provider.domain.DictDetail;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/28 11:55 上午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/dictDetail")
public class DictDetailController {
    private static final String ENTITY_NAME = "dictDetail";

    @Reference(version = "1.0.0")
    private DictDetailService dictDetailService;

    @GetMapping
    public ServerResponse<PageInfo> getDictDetails(@RequestParam(value = "dictName", defaultValue = "dept_status") String dictName, Page page) {
        IPage<DictDetail> iPage = dictDetailService.findDictDetail(dictName, page);
        return ServerResponse.createBySuccess("获取字典详情列表成功", PageFactory.createPageInfo(iPage));
    }

    @GetMapping(value = "/map")
    public ServerResponse<Map<String,Object>> getDictDetailMaps(String dictName){
        String[] names = dictName.split(",");
        Map<String,Object> map = new HashMap<>(names.length);
        for (String name : names) {
            map.put(name,dictDetailService.queryAll(name));
        }
        return ServerResponse.createBySuccess("获取字典映射成功", map);
    }

    @PostMapping
    @PreAuthorize("@pms.hasPermission('dict:add')")
    public ServerResponse create(@Validated @RequestBody DictDetail resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        dictDetailService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    @PreAuthorize("@pms.hasPermission('dict:edit')")
    public ServerResponse update(@RequestBody DictDetail resources){
        dictDetailService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@pms.hasPermission('dict:del')")
    public ServerResponse delete(@PathVariable Long id){
        dictDetailService.removeById(id);
        return ServerResponse.createBySuccessMessage("删除成功");
    }

}
