package com.gitee.zero.business.controller;

import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.security.util.SecurityUtils;
import com.gitee.zero.provider.api.MenuService;
import com.gitee.zero.provider.api.RolesMenusService;
import com.gitee.zero.provider.api.UserService;
import com.gitee.zero.provider.domain.Menu;
import com.gitee.zero.provider.domain.RolesMenus;
import com.gitee.zero.provider.domain.User;
import com.gitee.zero.provider.dto.MenuDto;
import com.gitee.zero.provider.vo.MenuVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Description: 菜单控制器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/2/16 10:09 上午
 * @see
 */
@Slf4j
@RestController
@RequestMapping("/api/menus")
public class MenuController {

    private static final String ENTITY_NAME = "menu";

    @Reference(version = "1.0.0")
    private MenuService menuService;

    @Reference(version = "1.0.0")
    private RolesMenusService rolesMenusService;

    @Reference(version = "1.0.0")
    private UserService userService;

    @GetMapping(value = "/build")
    public ServerResponse<List<MenuVo>> buildMenus(){
        User user = userService.get(SecurityUtils.getUserName());
        // 获取当前用户的角色列表
        List<RolesMenus> rolesMenusList = rolesMenusService.getMenusByRole(user.getId());
        List<Long> menuIds = new ArrayList<>();
        rolesMenusList.forEach(rolesMenus -> {
            menuIds.add(rolesMenus.getMenuId());
        });
        List<Menu> menuList = menuService.getMenus(menuIds);
        List<MenuDto> menuDtoList = new ArrayList<>();
        menuList.forEach(menu -> {
            MenuDto menuDto = new MenuDto();
            BeanUtils.copyProperties(menu, menuDto);
            menuDtoList.add(menuDto);
        });
        List<MenuDto> menuDtos = (List<MenuDto>) menuService.buildTree(menuDtoList).get("content");
        return ServerResponse.createBySuccess("获取菜单树成功", menuService.buildMenus(menuDtos));
    }

    @GetMapping(value = "/tree")
    public ServerResponse<List> getMenuTree(){
        List<Menu> menus = menuService.findByPid(0L);
        return ServerResponse.createBySuccess("获取菜单成功", menuService.getMenuTree(menus));
    }

    @GetMapping
    public ServerResponse<Map<String, Object>> getMenus(){
        List<Menu> menuList = menuService.list();
        List<MenuDto> menuDtoList = new ArrayList<>();
        menuList.forEach(menu -> {
            MenuDto menuDto = new MenuDto();
            BeanUtils.copyProperties(menu, menuDto);
            menuDtoList.add(menuDto);
        });
        return ServerResponse.createBySuccess("获取菜单成功", menuService.buildTree(menuDtoList));
    }

    @PostMapping
    @PreAuthorize("@pms.hasPermission('menu:add')")
    public ServerResponse create(@Validated @RequestBody Menu resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        menuService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    @PreAuthorize("@pms.hasPermission('menu:edit')")
    public ServerResponse update(@Validated @RequestBody Menu resources){
        menuService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }


    @DeleteMapping
    @PreAuthorize("@pms.hasPermission('menu:del')")
    public ServerResponse delete(@RequestBody Set<Long> ids){
        menuService.removeByIds(ids);
        return ServerResponse.createBySuccessMessage("删除成功");
    }
}
