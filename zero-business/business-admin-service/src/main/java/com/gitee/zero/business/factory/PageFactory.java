package com.gitee.zero.business.factory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gitee.zero.commons.dto.page.PageInfo;

/**
 * <p>
 * Description: vue 默认的分页参数创建
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/8 11:02 上午
 * @see
 */
public class PageFactory {
    public static PageInfo createPageInfo(IPage page) {
        PageInfo result = new PageInfo();
        result.setTotal(page.getTotal());
        result.setContent(page.getRecords());
        return result;
    }
}
