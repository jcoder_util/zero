package com.gitee.zero.business;

import com.gitee.zero.commons.security.annotation.EnableZeroResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>
 * Description: admin服务启动类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:47 下午
 * @see com.gitee.zero.cloud
 */
@EnableZeroResourceServer
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication(scanBasePackageClasses = {BusinessAdminServiceApplication.class}, scanBasePackages = {"com.gitee.zero.*.feign"})
public class BusinessAdminServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(BusinessAdminServiceApplication.class, args);
    }
}
