package com.gitee.zero.business.controller;

import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.TreeViewNode;
import com.gitee.zero.provider.api.DeptService;
import com.gitee.zero.provider.api.LogService;
import com.gitee.zero.provider.domain.Dept;
import com.gitee.zero.provider.domain.Log;
import com.gitee.zero.provider.domain.Menu;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * Description: 系统日志 控制器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/log")
public class LogController {

    private static final String ENTITY_NAME = "log";

    @Reference(version = "1.0.0")
    private LogService logService;

    @PostMapping
    public ServerResponse create(@Validated @RequestBody Log resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        logService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }
}