package com.gitee.zero.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zero.business.factory.PageFactory;
import com.gitee.zero.commons.core.exception.BadRequestException;
import com.gitee.zero.commons.dto.ServerResponse;
import com.gitee.zero.commons.dto.page.PageInfo;
import com.gitee.zero.provider.api.DictService;
import com.gitee.zero.provider.domain.Dict;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * Description: 字典管理
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/1 10:05 下午
 * @see com.gitee.zero.business.controller
 */
@RestController
@RequestMapping("/api/dict")
public class DictController {

    private static final String ENTITY_NAME = "dict";

    @Reference(version = "1.0.0")
    private DictService dictService;

    @GetMapping(value = "/all")
    @PreAuthorize("@pms.hasPermission('dict:list')")
    public ServerResponse<List<Dict>> all(){
        List<Dict> dictList = dictService.list();
        return ServerResponse.createBySuccess("获取字典列表成功", dictList);
    }

    @GetMapping
    @PreAuthorize("@pms.hasPermission('dict:list')")
    public ServerResponse<PageInfo> getDicts(Page page){
        IPage<Dict> iPage = dictService.page(page);
        return ServerResponse.createBySuccess("获取字典列表成功", PageFactory.createPageInfo(iPage));
    }


    @PostMapping
    @PreAuthorize("@pms.hasPermission('dict:add')")
    public ServerResponse create(@Validated @RequestBody Dict resources){
        if (resources.getId() != null) {
            throw new BadRequestException("A new "+ ENTITY_NAME +" cannot already have an ID");
        }
        dictService.save(resources);
        return ServerResponse.createBySuccessMessage("新增成功");
    }

    @PutMapping
    @PreAuthorize("@pms.hasPermission('dict:edit')")
    public ServerResponse update(@Validated @RequestBody Dict resources){
        dictService.updateById(resources);
        return ServerResponse.createBySuccessMessage("更新成功");
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@pms.hasPermission('dict:del')")
    public ServerResponse delete(@PathVariable Long id){
        dictService.removeById(id);
        return ServerResponse.createBySuccessMessage("删除成功");
    }
}
