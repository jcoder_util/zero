package com.gitee.zero.generator.core.generator;

import com.gitee.zero.generator.core.generator.param.ContextParam;

/**
 * <p>
 * Description: 代码生成器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/19 6:01 下午
 * @see com.gitee.zero.generator.core.generator
 */
public abstract class Generator {

    protected ContextParam contextParam;

    /**
     * 初始化配置
     *
     * @author fengshuonan
     * @Date 2018/12/12 3:13 PM
     */
    public void initContext(ContextParam paramContext) {
        this.contextParam = paramContext;
    }

    /**
     * 代码生成之前，自由发挥
     *
     * @author fengshuonan
     * @Date 2018/12/13 2:30 PM
     */
    protected void beforeGeneration() {

    }

    /**
     * 执行代码生成
     *
     * @author fengshuonan
     * @Date 2018/12/12 3:13 PM
     */
    public abstract void doGeneration();

    /**
     * 代码生成之后，自由发挥
     *
     * @author fengshuonan
     * @Date 2018/12/13 2:30 PM
     */
    protected void afterGeneration() {

    }

}
