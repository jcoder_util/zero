package com.gitee.zero.generator.core.engine;

import com.gitee.zero.generator.core.util.TemplateUtil;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.resource.ClasspathResourceLoader;

import java.io.IOException;
import java.util.Properties;

/**
 * <p>
 * Description: beetl模板引擎的实例
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/19 6:01 下午
 * @see com.gitee.zero.generator.core.engine
 */
public class BeetlEngine {

    private static BeetlEngine beetlEngine = new BeetlEngine();

    private static GroupTemplate groupTemplate;

    private BeetlEngine() {
        Properties properties = new Properties();
        properties.put("RESOURCE.root", "");
        properties.put("DELIMITER_STATEMENT_START", "<%");
        properties.put("DELIMITER_STATEMENT_END", "%>");
        properties.put("HTML_TAG_FLAG", "##");
        Configuration cfg = null;

        try {
            cfg = new org.beetl.core.Configuration(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
        groupTemplate = new GroupTemplate(resourceLoader, cfg);
        groupTemplate.registerFunctionPackage("tool", new TemplateUtil());
    }

    public static GroupTemplate getInstance() {
        return groupTemplate;
    }

}
