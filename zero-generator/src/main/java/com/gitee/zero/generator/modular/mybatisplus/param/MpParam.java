package com.gitee.zero.generator.modular.mybatisplus.param;

import lombok.Data;

/**
 * <p>
 * Description: 代码生成所需要的上下文参数
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/20 11:03 上午
 * @see com.gitee.zero.generator.modular.mybatisplus.param
 */
@Data
public class MpParam {

    /**
     * 移除表前缀
     */
    private String[] removeTablePrefix = {""};

    /**
     * 包含的表名称
     */
    private String[] includeTables;

    /**
     * 是否生成service的接口
     */
    private Boolean generatorInterface = false;

}
