package com.gitee.zero.generator.modular.vue;

import com.gitee.zero.generator.core.generator.AbstractCustomGenerator;
import org.beetl.core.Template;

import java.io.File;
import java.util.Map;

/**
 * <p>
 * Description: vue api 生成器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/20 5:42 下午
 * @see com.gitee.zero.generator.modular.vue
 */
public class VueApiGenerator extends AbstractCustomGenerator {

    public VueApiGenerator(Map<String, Object> tableContext) {
        super(tableContext);
    }

    @Override
    public void bindingOthers(Template template) {
        super.bindingInputsParams(template);
    }

    @Override
    public String getTemplateResourcePath() {
        return "/templates/api.btl";
    }

    @Override
    public String getGenerateFilePath() {
        String lowerEntity = (String) this.tableContext.get("lowerEntity");
        File file = new File(contextParam.getOutputPath() + "/vue/api/" + lowerEntity + ".js");
        return file.getAbsolutePath();
    }
}
