package com.gitee.zero.generator.modular;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.gitee.zero.generator.core.generator.param.ContextParam;
import com.gitee.zero.generator.modular.controller.ControllerGenerator;
import com.gitee.zero.generator.modular.mybatisplus.MpGenerator;
import com.gitee.zero.generator.modular.mybatisplus.param.MpParam;
import com.gitee.zero.generator.modular.vue.VueApiGenerator;
import com.gitee.zero.generator.modular.vue.VueIndexGenerator;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Description: 执行器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/20 3:35 下午
 * @see com.gitee.zero.generator.modular
 */
public class GeneratorExecutor {

    /**
     * 默认的生成器
     *
     * @author fengshuonan
     * @Date 2019/1/13 22:18
     */
    public static void executor(ContextParam contextParam, MpParam mpContext) {

        //执行mp的代码生成，生成entity,dao,service,model，生成后保留数据库元数据
        MpGenerator mpGenerator = new MpGenerator(mpContext);
        mpGenerator.initContext(contextParam);
        mpGenerator.doGeneration();

        //获取元数据
        List<TableInfo> tableInfos = mpGenerator.getTableInfos();
        Map<String, Map<String, Object>> everyTableContexts = mpGenerator.getEveryTableContexts();

        //遍历所有表
        for (TableInfo tableInfo : tableInfos) {
            Map<String, Object> map = everyTableContexts.get(tableInfo.getName());

            //生成控制器
            ControllerGenerator controllerGenerator = new ControllerGenerator(map);
            controllerGenerator.initContext(contextParam);
            controllerGenerator.doGeneration();

            //生成主页面index.vue
            VueIndexGenerator vueIndexGenerator = new VueIndexGenerator(map);
            vueIndexGenerator.initContext(contextParam);
            vueIndexGenerator.doGeneration();

            //生成接口调用api.js
            VueApiGenerator vueApiGenerator = new VueApiGenerator(map);
            vueApiGenerator.initContext(contextParam);
            vueApiGenerator.doGeneration();

             //生成菜单的sql
//            GunsMenuSqlGenerator gunsMenuSqlGenerator = new GunsMenuSqlGenerator(map);
//            gunsMenuSqlGenerator.initContext(contextParam);
//            gunsMenuSqlGenerator.doGeneration();
        }
    }

    public static void main(String[] args) {

        ContextParam contextParam = new ContextParam();

        contextParam.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        contextParam.setJdbcUserName("root");
        contextParam.setJdbcPassword("123456");
        contextParam.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/zero?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=CTT");
        contextParam.setOutputPath("temp");
        contextParam.setAuthor("张传臣");
        contextParam.setProPackage("com.gitee.zero.business");

        MpParam mpContextParam = new MpParam();
        mpContextParam.setGeneratorInterface(true);
        mpContextParam.setIncludeTables(new String[]{"dept"});
        mpContextParam.setRemoveTablePrefix(new String[]{""});

        GeneratorExecutor.executor(contextParam, mpContextParam);
    }

}
