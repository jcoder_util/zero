package com.gitee.zero.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>
 * Description: 代码生成器启动类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/19 5:25 下午
 * @see com.gitee.zero.generator
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GeneratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorApplication.class, args);
    }
}
