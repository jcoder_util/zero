package com.gitee.zero.generator.modular.controller;

import com.gitee.zero.generator.core.generator.AbstractCustomGenerator;
import org.beetl.core.Template;

import java.io.File;
import java.util.Map;

/**
 * <p>
 * Description: 接口控制器生成器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/20 9:49 上午
 * @see com.gitee.zero.generator.modular.controller
 */
public class ControllerGenerator extends AbstractCustomGenerator {

    public ControllerGenerator(Map<String, Object> tableContext) {
        super(tableContext);
    }

    @Override
    public void bindingOthers(Template template) {
        template.binding("controllerPackage", contextParam.getProPackage() + ".controller");
    }

    @Override
    public String getTemplateResourcePath() {
        return "/templates/controller.java.btl";
    }

    @Override
    public String getGenerateFilePath() {
        String proPackage = this.contextParam.getProPackage();
        String proPath = proPackage.replaceAll("\\.", "/");
        File file = new File(contextParam.getOutputPath() + "/" + proPath + "/controller/" + tableContext.get("entity") + "Controller.java");
        return file.getAbsolutePath();
    }
}
