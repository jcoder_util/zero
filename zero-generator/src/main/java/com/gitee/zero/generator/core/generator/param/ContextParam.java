package com.gitee.zero.generator.core.generator.param;

import lombok.Data;

/**
 * <p>
 * Description: 代码生成所需要的上下文参数
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/19 6:01 下午
 * @see com.gitee.zero.generator.core.generator.param
 */
@Data
public class ContextParam {

    /**
     * jdbc连接的驱动名称
     */
    private String jdbcDriver;

    /**
     * 数据库连接的url
     */
    private String jdbcUrl;

    /**
     * 数据库用户名
     */
    private String jdbcUserName;

    /**
     * 数据库连接的密码
     */
    private String jdbcPassword;

    /**
     * 代码生成路径
     */
    private String outputPath = "temp";

    /**
     * 项目的包路径
     */
    private String proPackage;

    /**
     * 代码生成作者
     */
    private String author = "张传臣";

}
