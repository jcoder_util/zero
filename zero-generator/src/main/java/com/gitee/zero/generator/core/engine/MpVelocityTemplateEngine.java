package com.gitee.zero.generator.core.engine;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.config.ConstVal;
import com.baomidou.mybatisplus.generator.config.builder.ConfigBuilder;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.Properties;

/**
 * <p>
 * Description: 重写Mybatis-Plus的Velocity模板引擎实现
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/5/19 6:01 下午
 * @see com.gitee.zero.generator.core.engine
 */
//public class MpVelocityTemplateEngine extends VelocityTemplateEngine {
//
//    private static final String DOT_VM = ".vm";
//    private VelocityEngine velocityEngine;
//
//    @Override
//    public VelocityTemplateEngine init(ConfigBuilder configBuilder) {
//        super.init(configBuilder);
//        if (null == velocityEngine) {
//            Properties p = new Properties();
//            p.setProperty(ConstVal.VM_LOAD_PATH_KEY, ConstVal.VM_LOAD_PATH_VALUE);
//            p.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, StringPool.EMPTY);
//            p.setProperty(Velocity.ENCODING_DEFAULT, ConstVal.UTF8);
//            p.setProperty(Velocity.INPUT_ENCODING, ConstVal.UTF8);
//            p.setProperty("file.resource.loader.unicode", StringPool.TRUE);
//            velocityEngine = new VelocityEngine(p);
//        }
//        return this;
//    }
//
//
//    @Override
//    public void writer(Map<String, Object> objectMap, String templatePath, String outputFile) throws Exception {
//        if (StringUtils.isEmpty(templatePath)) {
//            return;
//        }
//        Template template = velocityEngine.getTemplate(templatePath, ConstVal.UTF8);
//        try (FileOutputStream fos = new FileOutputStream(outputFile);
//             OutputStreamWriter ow = new OutputStreamWriter(fos, ConstVal.UTF8);
//             BufferedWriter writer = new BufferedWriter(ow)) {
//            template.merge(new VelocityContext(objectMap), writer);
//        }
//        logger.debug("模板:" + templatePath + ";  文件:" + outputFile);
//    }
//
//
//    @Override
//    public String templateFilePath(String filePath) {
//        if (null == filePath || filePath.contains(DOT_VM)) {
//            return filePath;
//        }
//        return filePath + DOT_VM;
//    }
//
//    /**
//     * 重写父类的方法，增加一个变量，为了在mapping.xml的Base_Column_List增加base前缀
//     *
//     * @author fengshuonan
//     * @Date 2019/1/10 12:51 PM
//     */
//    @Override
//    public Map<String, Object> getObjectMap(TableInfo tableInfo) {
//        Map<String, Object> objectMap = super.getObjectMap(tableInfo);
//        objectMap.put("tableRebuild", TableInfoUtil.getFieldNames(tableInfo));
//        return objectMap;
//    }
//}
