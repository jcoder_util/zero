package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.User;

/**
 * <p>
 * Description: 系统用户服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface UserService extends IService<User> {
    /**
     * 获取用户
     *
     * @param userName 用户名
     * @return {@link User}
     */
    User get(String userName);

}
