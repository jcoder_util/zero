package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.RolesMenus;

import java.util.List;

/**
 * <p>
 * Description: 角色菜单关联服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface RolesMenusService extends IService<RolesMenus> {

    /**
     * 根据角色获取菜单资源
     * @param userId
     * @return
     */
    List<RolesMenus> getMenusByRole(Long userId);

    /**
     * 根据角色删除菜单
     *
     * @param roleId
     * @return
     */
    int removeMenusByRole(Long roleId);
}
