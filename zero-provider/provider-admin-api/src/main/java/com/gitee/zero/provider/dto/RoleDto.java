package com.gitee.zero.provider.dto;

import com.gitee.zero.provider.domain.Dept;
import com.gitee.zero.provider.domain.Menu;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * Description: RoleDto
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/29 10:17 下午
 * @see com.gitee.zero.provider.dto
 */
@Data
public class RoleDto implements Serializable {

    private Long id;

    private String name;

    private String dataScope;

    private Integer level;

    private String remark;

    private String permission;

    private List<Menu> menus;

    private Set<Dept> depts;

    private Date createTime;
}
