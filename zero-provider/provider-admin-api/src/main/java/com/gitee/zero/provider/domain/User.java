package com.gitee.zero.provider.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * Description: 系统用户
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.domain
 */
@Data
@ToString
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 状态：1启用、0禁用
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 用户名
     */
    @TableField("username")
    private String username;

    /**
     * 部门名称
     */
    @TableField("dept_id")
    private Long deptId;

    /**
     * 手机号码
     */
    @TableField("phone")
    private String phone;

    /**
     * 岗位名称
     */
    @TableField("job_id")
    private Long jobId;

    /**
     * 创建日期
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后修改密码的日期
     */
    @TableField("last_password_reset_time")
    private Date lastPasswordResetTime;

    @TableField("nick_name")
    private String nickName;

    @TableField("sex")
    private String sex;
}
