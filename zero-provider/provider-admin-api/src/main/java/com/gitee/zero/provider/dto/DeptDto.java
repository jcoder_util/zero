package com.gitee.zero.provider.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/28 11:02 上午
 * @see com.gitee.zero.provider.dto
 */
@Data
public class DeptDto implements Serializable {

    private Long id;

    private String name;

    @NotNull
    private Long enabled;

    private Long pid;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<DeptDto> children;

    private Timestamp createTime;

    public String getLabel() {
        return name;
    }
}
