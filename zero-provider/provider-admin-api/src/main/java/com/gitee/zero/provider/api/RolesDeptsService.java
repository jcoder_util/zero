package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.RolesDepts;

import java.util.List;

/**
 * <p>
 * Description: 角色部门关联服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface RolesDeptsService extends IService<RolesDepts> {
}
