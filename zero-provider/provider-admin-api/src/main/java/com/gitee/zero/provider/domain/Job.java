package com.gitee.zero.provider.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * Description: 岗位
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.domain
 */
@TableName("job")
public class Job implements Serializable {

    private static final long serialVersionUID = 111L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 岗位名称
     */
    @TableField("name")
    private String name;

    /**
     * 岗位状态
     */
    @TableField("enabled")
    private Long enabled;

    /**
     * 岗位排序
     */
    @TableField("sort")
    private Long sort;

    /**
     * 部门ID
     */
    @TableField("dept_id")
    private Long deptId;

    /**
     * 创建日期
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEnabled() {
        return enabled;
    }

    public void setEnabled(Long enabled) {
        this.enabled = enabled;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Job{" +
        "id=" + id +
        ", name=" + name +
        ", enabled=" + enabled +
        ", sort=" + sort +
        ", deptId=" + deptId +
        ", createTime=" + createTime +
        "}";
    }
}
