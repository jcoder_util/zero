package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.UsersRoles;

/**
 * <p>
 * Description: 用户角色关联服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface UsersRolesService extends IService<UsersRoles> {

}
