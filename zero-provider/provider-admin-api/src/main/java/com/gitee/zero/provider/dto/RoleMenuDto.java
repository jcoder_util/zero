package com.gitee.zero.provider.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * <p>
 * Description: 角色与菜单数据存储转换类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/18 9:05 上午
 * @see com.gitee.zero.provider.dto
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleMenuDto {
    private Long roleId;
    private Set<Long> menus;
}
