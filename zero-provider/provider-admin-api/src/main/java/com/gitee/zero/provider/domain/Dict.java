package com.gitee.zero.provider.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * Description: 数据字典
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.domain
 */
@TableName("dict")
public class Dict implements Serializable {

    private static final long serialVersionUID = 12L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 字典名称
     */
    @TableField("name")
    private String name;

    /**
     * 描述
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建日期
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Dict{" +
        "id=" + id +
        ", name=" + name +
        ", remark=" + remark +
        ", createTime=" + createTime +
        "}";
    }
}
