package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.Role;
import com.gitee.zero.provider.dto.RoleDto;
import com.gitee.zero.provider.dto.UserDto;
//import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Description: 角色表服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface RoleService extends IService<Role> {
    /**
     * 获取用户权限信息
     * @param user 用户信息
     * @return 权限信息
     */
//    Collection<GrantedAuthority> mapToGrantedAuthorities(UserDto user);

    /**
     * 获取单个role
     *
     * @param role
     * @return
     */
    RoleDto getRoleDto(Role role);

    /**
     * listRoleDto
     *
     * @return
     */
    List<RoleDto> listRoleDto();

    /**
     * @param page
     * @return
     */
    IPage<RoleDto> pageRoleDto(Page page);

}
