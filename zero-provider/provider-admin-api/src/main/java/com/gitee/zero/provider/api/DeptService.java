package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.commons.dto.TreeViewNode;
import com.gitee.zero.provider.domain.Dept;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Description: 部门服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface DeptService extends IService<Dept> {

    /**
     * 获取部门树
     * @return
     */
    List<TreeViewNode> findTreeViewNode();

    /**
     * 构建树形数据
     * @param treeViewNodeList 原始数据
     * @return /
     */
    Map<String, Object> buildTree(List<TreeViewNode> treeViewNodeList);


}
