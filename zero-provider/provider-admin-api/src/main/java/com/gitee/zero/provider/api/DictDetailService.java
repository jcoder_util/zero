package com.gitee.zero.provider.api;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.DictDetail;

import java.util.List;

/**
 * <p>
 * Description: 数据字典详情服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface DictDetailService extends IService<DictDetail> {

    /**
     *
     * @param dictName
     * @param page
     * @return
     */
    IPage<DictDetail> findDictDetail(String dictName, Page page);

    /**
     *
     * @param dictName
     * @return
     */
    List<DictDetail> queryAll(String dictName);
}
