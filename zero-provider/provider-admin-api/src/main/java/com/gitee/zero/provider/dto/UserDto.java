package com.gitee.zero.provider.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

/**
 * <p>
 * Description: 数据字典
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.dto
 */
@Data
public class UserDto implements Serializable {
    private static final long serialVersionUID = 104L;

    private Long id;

    private String username;

    private String nickName;

    private String sex;

    private String avatar;

    private String email;

    private String phone;

    private Boolean enabled;

    @JsonIgnore
    private String password;

    private Date lastPasswordResetTime;

    private Set<RoleSmallDto> roles;

    private DeptSmallDto dept;

    private Long deptId;

    private Timestamp createTime;
}
