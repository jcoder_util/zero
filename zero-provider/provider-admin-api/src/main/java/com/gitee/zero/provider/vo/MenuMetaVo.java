package com.gitee.zero.provider.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * Description: 菜单
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.vo
 */
@Data
@AllArgsConstructor
public class MenuMetaVo implements Serializable {

    private static final long serialVersionUID = 105L;

    private String title;

    private String icon;

    private Boolean noCache;
}
