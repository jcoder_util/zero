package com.gitee.zero.provider.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * Description: 系统菜单
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.domain
 */
@TableName("menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 16L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 是否外链
     */
    @TableField("i_frame")
    private Boolean iFrame;

    /**
     * 菜单名称
     */
    @TableField("name")
    private String name;

    /**
     * 组件
     */
    @TableField("component")
    private String component;

    /**
     * 上级菜单ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 排序
     */
    @TableField("sort")
    private Long sort;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 链接地址
     */
    @TableField("path")
    private String path;

    /**
     * 缓存
     */
    @TableField("cache")
    private Boolean cache;

    /**
     * 隐藏
     */
    @TableField("hidden")
    private Boolean hidden;

    /**
     * 组件名称
     */
    @TableField("component_name")
    private String componentName;

    /**
     * 权限
     */
    @TableField("permission")
    private String permission;

    /**
     * 类型
     */
    @TableField("type")
    private Integer type;

    /**
     * 创建日期
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getiFrame() {
        return iFrame;
    }

    public void setiFrame(Boolean iFrame) {
        this.iFrame = iFrame;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Menu{" +
        "id=" + id +
        ", iFrame=" + iFrame +
        ", name=" + name +
        ", component=" + component +
        ", pid=" + pid +
        ", sort=" + sort +
        ", icon=" + icon +
        ", path=" + path +
        ", cache=" + cache +
        ", hidden=" + hidden +
        ", componentName=" + componentName +
        ", createTime=" + createTime +
        ", permission=" + permission +
        ", type=" + type +
        "}";
    }
}
