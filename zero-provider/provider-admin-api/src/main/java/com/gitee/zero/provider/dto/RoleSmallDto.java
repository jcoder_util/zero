package com.gitee.zero.provider.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * Description: 角色
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.dto
 */
@Data
public class RoleSmallDto implements Serializable {
    private static final long serialVersionUID = 103L;

    private Long id;

    private String name;

    private Integer level;

    private String dataScope;
}
