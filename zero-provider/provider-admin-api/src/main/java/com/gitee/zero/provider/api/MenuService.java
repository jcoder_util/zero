package com.gitee.zero.provider.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.zero.provider.domain.Menu;
import com.gitee.zero.provider.dto.MenuDto;
import com.gitee.zero.provider.vo.MenuVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Description: 系统菜单服务类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.api
 */
public interface MenuService extends IService<Menu> {

    /**
     * 构建菜单树
     * @return /
     */
    Map<String,Object> buildTree(List<MenuDto> menuDtos);

    /**
     * 构建菜单树
     * @param menuDtoList /
     * @return /
     */
    List<MenuVo> buildMenus(List<MenuDto> menuDtoList);

    /**
     * 获取菜单
     * @param menuIds
     * @return
     */
    List<Menu> getMenus(List<Long> menuIds);

    /**
     * 获取菜单
     * @param menuIds
     * @return
     */
    List<Menu> getAllMenus(List<Long> menuIds);

    /**
     * 根据pid查询
     * @param pid
     * @return
     */
    List<Menu> findByPid(long pid);

    /**
     * 获取菜单树
     * @param menus
     * @return
     */
    List getMenuTree(List<Menu> menus);
}
