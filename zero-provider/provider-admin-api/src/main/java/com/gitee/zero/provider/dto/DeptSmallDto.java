package com.gitee.zero.provider.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * Description: 部门
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/3/31 3:13 下午
 * @see com.gitee.zero.provider.dto
 */
@Data
public class DeptSmallDto implements Serializable {
    private static final long serialVersionUID = 101L;

    private Long id;

    private String name;
}