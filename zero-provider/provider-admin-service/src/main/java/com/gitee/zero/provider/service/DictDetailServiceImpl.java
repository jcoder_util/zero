package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.DictDetailService;
import com.gitee.zero.provider.api.DictService;
import com.gitee.zero.provider.domain.Dict;
import com.gitee.zero.provider.domain.DictDetail;
import com.gitee.zero.provider.mapper.DictDetailMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * Description: 数据字典详情 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class DictDetailServiceImpl extends ServiceImpl<DictDetailMapper, DictDetail> implements DictDetailService {

    @Resource
    private DictService dictService;

    @Override
    public IPage<DictDetail> findDictDetail(String dictName, Page page) {
        Dict dict = dictService.getOne(new QueryWrapper<Dict>().eq("name", dictName));
        return this.baseMapper.selectPage(page, new QueryWrapper<DictDetail>().eq("dict_id", dict.getId()));
    }

    @Override
    public List<DictDetail> queryAll(String dictName) {
        Dict dict = dictService.getOne(new QueryWrapper<Dict>().eq("name", dictName));
        return this.baseMapper.selectList(new QueryWrapper<DictDetail>().eq("dict_id", dict.getId()));
    }
}
