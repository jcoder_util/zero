package com.gitee.zero.provider.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.RoleService;
import com.gitee.zero.provider.domain.Menu;
import com.gitee.zero.provider.domain.Role;
import com.gitee.zero.provider.domain.RolesMenus;
import com.gitee.zero.provider.dto.RoleDto;
import com.gitee.zero.provider.dto.UserDto;
import com.gitee.zero.provider.mapper.MenuMapper;
import com.gitee.zero.provider.mapper.RoleMapper;
import com.gitee.zero.provider.mapper.RolesMenusMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * Description: 角色表 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Resource
    private MenuMapper menuMapper;
    @Resource
    private RolesMenusMapper rolesMenusMapper;

//    @Override
//    public Collection<GrantedAuthority> mapToGrantedAuthorities(UserDto user) {
////        List<Role> roles = this.baseMapper.selectList(new QueryWrapper<Role>().eq("", user.getId()));
////        Set<String> permissions = roles.stream().filter(role -> StringUtils.isNotBlank(role.getPermission())).map(Role::getPermission).collect(Collectors.toSet());
////        permissions.addAll(
////                roles.stream().flatMap(role -> role.getMenus().stream())
////                        .filter(menu -> StringUtils.isNotBlank(menu.getPermission()))
////                        .map(Menu::getPermission).collect(Collectors.toSet())
////        );
////        return permissions.stream().map(SimpleGrantedAuthority::new)
////                .collect(Collectors.toList());
//        return null;
//    }

    @Override
    public List<RoleDto> listRoleDto() {
        List<RoleDto> roleDtoList = new ArrayList<>();
        List<Role> roleList = this.baseMapper.selectList(null);
        roleList.forEach(role -> {
            RoleDto roleDto = getRoleDto(role);
            roleDtoList.add(roleDto);
        });
        return roleDtoList;
    }
    @Override
    public RoleDto getRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        BeanUtils.copyProperties(role, roleDto);
        List<RolesMenus> rolesMenusList = rolesMenusMapper.selectList(
                new QueryWrapper<RolesMenus>().eq("role_id", role.getId()));
        List<Long> menuIds = new ArrayList<>();
        rolesMenusList.forEach(rolesMenus -> {
            menuIds.add(rolesMenus.getMenuId());
        });
        List<Menu> menus = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(menuIds) && menuIds.size() > 0) {
            menus = menuMapper.selectList(new QueryWrapper<Menu>().in("id", menuIds));
        }
        roleDto.setMenus(menus);
        return roleDto;
    }

    @Override
    public IPage<RoleDto> pageRoleDto(Page page) {
        List<RoleDto> roleDtoList = new ArrayList<>();
        IPage iPage = this.baseMapper.selectPage(page,null);
        List<Role> roleList = iPage.getRecords();
        roleList.forEach(role -> {
            RoleDto roleDto = getRoleDto(role);
            roleDtoList.add(roleDto);
        });
        IPage<RoleDto> roleDtoIPage = new Page<>();
        roleDtoIPage.setRecords(roleDtoList);
        roleDtoIPage.setSize(iPage.getSize());
        roleDtoIPage.setTotal(iPage.getTotal());
        roleDtoIPage.setCurrent(iPage.getCurrent());
        roleDtoIPage.setPages(iPage.getPages());
        System.out.println(roleDtoIPage.toString());
        return roleDtoIPage;

    }
}
