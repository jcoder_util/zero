package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.RolesDeptsService;
import com.gitee.zero.provider.domain.RolesDepts;
import com.gitee.zero.provider.mapper.RolesDeptsMapper;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 * Description: 角色部门关联 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class RolesDeptsServiceImpl extends ServiceImpl<RolesDeptsMapper, RolesDepts> implements RolesDeptsService {

}
