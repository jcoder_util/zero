package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.UserService;
import com.gitee.zero.provider.domain.User;
import com.gitee.zero.provider.mapper.UserMapper;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 * Description: 系统用户 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User get(String userName) {
        return baseMapper.selectOne(new QueryWrapper<User>().eq("username", userName));
    }
}
