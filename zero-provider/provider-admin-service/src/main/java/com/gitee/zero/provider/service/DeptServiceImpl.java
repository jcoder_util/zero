package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.commons.dto.TreeViewNode;
import com.gitee.zero.provider.api.DeptService;
import com.gitee.zero.provider.domain.Dept;
import com.gitee.zero.provider.mapper.DeptMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * Description: 部门 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

    private static final String P_TREE_NODE = "0";

    @Override
    public List<TreeViewNode> findTreeViewNode() {
        List<Dept> deptList = this.list();
        List<TreeViewNode> treeViewNodes = new ArrayList<>();
        deptList.forEach(dept -> {
            TreeViewNode treeViewNode = new TreeViewNode();
            BeanUtils.copyProperties(dept, treeViewNode);
            treeViewNodes.add(treeViewNode);
        });
        return treeViewNodes;
    }

    @Override
    public Map<String, Object> buildTree(List<TreeViewNode> treeViewNodeList) {
        Set<TreeViewNode> trees = new LinkedHashSet<>();
        Set<TreeViewNode> depts = new LinkedHashSet<>();
        List<String> deptNames = treeViewNodeList.stream().map(TreeViewNode::getName).collect(Collectors.toList());
        boolean isChild;
        for (TreeViewNode treeViewNode : treeViewNodeList) {
            isChild = false;
            if (P_TREE_NODE.equals(treeViewNode.getPid().toString())) {
                trees.add(treeViewNode);
            }
            for (TreeViewNode it : treeViewNodeList) {
                if (it.getPid().equals(treeViewNode.getId())) {
                    isChild = true;
                    if (treeViewNode.getChildren() == null) {
                        treeViewNode.setChildren(new ArrayList<>());
                    }
                    treeViewNode.getChildren().add(it);
                }
            }
            if (isChild) {
                depts.add(treeViewNode);
            } else if (!deptNames.contains(this.baseMapper.selectById(treeViewNode.getPid()).getName())) {
                depts.add(treeViewNode);
            }
        }

        if (CollectionUtils.isEmpty(trees)) {
            trees = depts;
        }

        Integer totalElements = treeViewNodeList.size();

        Map<String, Object> map = new HashMap<>(2);
        map.put("totalElements", totalElements);
        map.put("content", CollectionUtils.isEmpty(trees) ? treeViewNodeList : trees);
        return map;
    }
}
