package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.UsersRolesService;
import com.gitee.zero.provider.domain.UsersRoles;
import com.gitee.zero.provider.mapper.UsersRolesMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Description: 用户角色关联 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service
public class UsersRolesServiceImpl extends ServiceImpl<UsersRolesMapper, UsersRoles> implements UsersRolesService {
}
