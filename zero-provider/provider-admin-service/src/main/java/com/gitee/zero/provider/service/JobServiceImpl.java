package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.JobService;
import com.gitee.zero.provider.domain.Job;
import com.gitee.zero.provider.mapper.JobMapper;
import org.apache.dubbo.config.annotation.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * Description: 岗位 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class JobServiceImpl extends ServiceImpl<JobMapper, Job> implements JobService {

    @Override
    public List<Job> findJobs() {
        return this.baseMapper.selectList(null);
    }
}
