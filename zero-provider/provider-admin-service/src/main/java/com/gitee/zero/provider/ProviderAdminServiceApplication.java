package com.gitee.zero.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>
 * Description: Admin服务提供者启动类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(basePackages = "com.gitee.zero.provider.mapper")
//@ComponentScan(basePackages = {"com.gitee.zero.provider.mapper"})
public class ProviderAdminServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderAdminServiceApplication.class, args);
    }

}
