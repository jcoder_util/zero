package com.gitee.zero.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.zero.provider.domain.DictDetail;

/**
 * <p>
 * Description: 数据字典详情 Mapper 接口
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.mapper
 */
public interface DictDetailMapper extends BaseMapper<DictDetail> {

}
