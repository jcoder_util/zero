package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.LogService;
import com.gitee.zero.provider.domain.Log;
import com.gitee.zero.provider.mapper.LogMapper;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 * Description: 系统日志 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {
}
