package com.gitee.zero.provider.configure;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * Description:
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.configure
 */
@Slf4j
@Component
public class ZeroMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        this.strictUpdateFill(metaObject, "updateTime", Date.class, LocalDateTime.now());
    }
}
