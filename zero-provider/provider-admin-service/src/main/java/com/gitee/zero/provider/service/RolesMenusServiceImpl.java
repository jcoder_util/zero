package com.gitee.zero.provider.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gitee.zero.provider.api.RolesMenusService;
import com.gitee.zero.provider.domain.RolesMenus;
import com.gitee.zero.provider.domain.UsersRoles;
import com.gitee.zero.provider.mapper.RolesMenusMapper;
import com.gitee.zero.provider.mapper.UsersRolesMapper;
import org.apache.dubbo.config.annotation.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * Description: 角色菜单关联 服务实现类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/17 3:36 下午
 * @see com.gitee.zero.provider.service
 */
@Service(version = "1.0.0")
public class RolesMenusServiceImpl extends ServiceImpl<RolesMenusMapper, RolesMenus> implements RolesMenusService {

    @Resource
    private UsersRolesMapper usersRolesMapper;

    @Override
    public List<RolesMenus> getMenusByRole(Long userId) {
        UsersRoles usersRoles = usersRolesMapper.selectOne(new QueryWrapper<UsersRoles>().eq("user_id", userId));
        return this.baseMapper.selectList(new QueryWrapper<RolesMenus>().eq("role_id", usersRoles.getRoleId()));
    }

    @Override
    public int removeMenusByRole(Long roleId) {
        return this.baseMapper.delete(new QueryWrapper<RolesMenus>().eq("role_id", roleId));
    }

}
