/*
 Navicat Premium Data Transfer

 Source Server         : cczhang
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : 59.110.125.108
 Source Database       : zero

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : utf-8

 Date: 04/26/2020 21:57:08 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `dept`
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `pid` bigint(20) NOT NULL COMMENT '上级部门',
  `enabled` bigint(20) NOT NULL COMMENT '状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='部门';

-- ----------------------------
--  Records of `dept`
-- ----------------------------
BEGIN;
INSERT INTO `dept` VALUES ('1', 'EL-ADMIN', '0', '1', '2019-03-01 12:07:37'), ('2', '研发部', '7', '1', '2019-03-25 09:15:32'), ('5', '运维部', '7', '1', '2019-03-25 09:20:44'), ('6', '测试部', '8', '1', '2019-03-25 09:52:18'), ('7', '华南分部', '1', '1', '2019-03-25 11:04:50'), ('8', '华北分部', '1', '1', '2019-03-25 11:04:53'), ('11', '人事部', '8', '1', '2019-03-25 11:07:58');
COMMIT;

-- ----------------------------
--  Table structure for `dict`
-- ----------------------------
DROP TABLE IF EXISTS `dict`;
CREATE TABLE `dict` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '字典名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据字典';

-- ----------------------------
--  Records of `dict`
-- ----------------------------
BEGIN;
INSERT INTO `dict` VALUES ('1', 'user_status', '用户状态', '2019-10-27 20:31:36'), ('4', 'dept_status', '部门状态', '2019-10-27 20:31:36'), ('5', 'job_status', '岗位状态', '2019-10-27 20:31:36');
COMMIT;

-- ----------------------------
--  Table structure for `dict_detail`
-- ----------------------------
DROP TABLE IF EXISTS `dict_detail`;
CREATE TABLE `dict_detail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL COMMENT '字典标签',
  `value` bigint(11) NOT NULL COMMENT '字典值',
  `sort` varchar(255) DEFAULT NULL COMMENT '排序',
  `dict_id` bigint(11) DEFAULT NULL COMMENT '字典id',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK5tpkputc6d9nboxojdbgnpmyb` (`dict_id`) USING BTREE,
  CONSTRAINT `FK5tpkputc6d9nboxojdbgnpmyb` FOREIGN KEY (`dict_id`) REFERENCES `dict` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据字典详情';

-- ----------------------------
--  Records of `dict_detail`
-- ----------------------------
BEGIN;
INSERT INTO `dict_detail` VALUES ('1', '激活', '1', '1', '1', '2019-10-27 20:31:36'), ('2', '禁用', '0', '2', '1', '2020-04-16 21:57:06'), ('3', '启用', '1', '1', '4', '2020-04-17 18:15:31'), ('4', '停用', '0', '2', '4', '2019-10-27 20:31:36'), ('5', '启用', '1', '1', '5', '2020-04-16 21:57:01'), ('6', '停用', '0', '2', '5', '2019-10-27 20:31:36');
COMMIT;

-- ----------------------------
--  Table structure for `job`
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT '岗位名称',
  `enabled` bigint(20) NOT NULL COMMENT '岗位状态',
  `sort` bigint(20) NOT NULL COMMENT '岗位排序',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKmvhj0rogastlctflsxf1d6k3i` (`dept_id`) USING BTREE,
  CONSTRAINT `FKmvhj0rogastlctflsxf1d6k3i` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='岗位';

-- ----------------------------
--  Records of `job`
-- ----------------------------
BEGIN;
INSERT INTO `job` VALUES ('8', '人事专员', '1', '3', '11', '2019-03-29 14:52:28'), ('10', '产品经理', '1', '4', '2', '2019-03-29 14:55:51'), ('11', '全栈开发', '1', '2', '2', '2019-03-31 13:39:30'), ('12', '软件测试', '1', '5', '2', '2019-03-31 13:39:43'), ('13', '软件测试1', '1', '6', '2', '2020-04-08 11:14:10'), ('27', 'rsea', '0', '999', '1', '2020-04-17 16:33:58');
COMMIT;

-- ----------------------------
--  Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `exception_detail` text,
  `log_type` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `params` text,
  `request_ip` varchar(255) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统日志';

-- ----------------------------
--  Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `i_frame` bit(1) DEFAULT NULL COMMENT '是否外链',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `component` varchar(255) DEFAULT NULL COMMENT '组件',
  `pid` bigint(20) NOT NULL COMMENT '上级菜单ID',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `path` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `cache` bit(1) DEFAULT b'0' COMMENT '缓存',
  `hidden` bit(1) DEFAULT b'0' COMMENT '隐藏',
  `component_name` varchar(20) DEFAULT '-' COMMENT '组件名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FKqcf9gem97gqa5qjm4d3elcqt5` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统菜单';

-- ----------------------------
--  Records of `menu`
-- ----------------------------
BEGIN;
INSERT INTO `menu` VALUES ('1', b'0', '系统管理', null, '0', '1', 'system', 'system', b'0', b'0', null, '2018-12-18 15:11:29', null, '0'), ('2', b'0', '用户管理', 'system/user/index', '1', '2', 'peoples', 'user', b'0', b'0', 'User', '2018-12-18 15:14:44', 'user:list', '1'), ('3', b'0', '角色管理', 'system/role/index', '1', '3', 'role', 'role', b'0', b'0', 'Role', '2018-12-18 15:16:07', 'roles:list', '1'), ('5', b'0', '菜单管理', 'system/menu/index', '1', '5', 'menu', 'menu', b'0', b'0', 'Menu', '2018-12-18 15:17:28', 'menu:list', '1'), ('6', b'0', '系统监控', null, '0', '10', 'monitor', 'monitor', b'0', b'0', null, '2018-12-18 15:17:48', null, '0'), ('7', b'0', '操作日志', 'monitor/log/index', '6', '11', 'log', 'logs', b'0', b'0', 'Log', '2018-12-18 15:18:26', null, '1'), ('9', b'0', 'SQL监控', 'monitor/sql/index', '6', '18', 'sqlMonitor', 'druid', b'0', b'0', 'Sql', '2018-12-18 15:19:34', null, '1'), ('10', b'0', '组件管理', null, '0', '50', 'zujian', 'components', b'0', b'0', null, '2018-12-19 13:38:16', null, '0'), ('11', b'0', '图标库', 'components/icons/index', '10', '51', 'icon', 'icon', b'0', b'0', 'Icons', '2018-12-19 13:38:49', null, '1'), ('14', b'0', '邮件工具', 'tools/email/index', '36', '35', 'email', 'email', b'0', b'0', 'Email', '2018-12-27 10:13:09', null, '1'), ('15', b'0', '富文本', 'components/Editor', '10', '52', 'fwb', 'tinymce', b'0', b'0', 'Editor', '2018-12-27 11:58:25', null, '1'), ('16', b'0', '图床管理', 'tools/picture/index', '36', '33', 'image', 'pictures', b'0', b'0', 'Pictures', '2018-12-28 09:36:53', 'pictures:list', '1'), ('18', b'0', '存储管理', 'tools/storage/index', '36', '34', 'qiniu', 'storage', b'0', b'0', 'Storage', '2018-12-31 11:12:15', 'storage:list', '1'), ('19', b'0', '支付宝工具', 'tools/aliPay/index', '36', '37', 'alipay', 'aliPay', b'0', b'0', 'AliPay', '2018-12-31 14:52:38', null, '1'), ('21', b'0', '多级菜单', '', '0', '900', 'menu', 'nested', b'0', b'1', null, '2019-01-04 16:22:03', null, '0'), ('22', b'0', '二级菜单1', 'nested/menu1/index', '21', '999', 'menu', 'menu1', b'0', b'0', null, '2019-01-04 16:23:29', null, '1'), ('23', b'0', '二级菜单2', 'nested/menu2/index', '21', '999', 'menu', 'menu2', b'0', b'0', null, '2019-01-04 16:23:57', null, '1'), ('24', b'0', '三级菜单1', 'nested/menu1/menu1-1', '22', '999', 'menu', 'menu1-1', b'0', b'0', null, '2019-01-04 16:24:48', null, '1'), ('27', b'0', '三级菜单2', 'nested/menu1/menu1-2', '22', '999', 'menu', 'menu1-2', b'0', b'0', null, '2019-01-07 17:27:32', null, '1'), ('28', b'0', '定时任务', 'system/timing/index', '36', '31', 'timing', 'timing', b'0', b'0', 'Timing', '2019-01-07 20:34:40', 'timing:list', '1'), ('30', b'0', '代码生成', 'generator/index', '36', '32', 'dev', 'generator', b'1', b'0', 'GeneratorIndex', '2019-01-11 15:45:55', null, '1'), ('32', b'0', '异常日志', 'monitor/log/errorLog', '6', '12', 'error', 'errorLog', b'0', b'0', 'ErrorLog', '2019-01-13 13:49:03', null, '1'), ('33', b'0', 'Markdown', 'components/MarkDown', '10', '53', 'markdown', 'markdown', b'0', b'0', 'Markdown', '2019-03-08 13:46:44', null, '1'), ('34', b'0', 'Yaml编辑器', 'components/YamlEdit', '10', '54', 'dev', 'yaml', b'0', b'0', 'YamlEdit', '2019-03-08 15:49:40', null, '1'), ('35', b'0', '部门管理', 'system/dept/index', '1', '6', 'dept', 'dept', b'0', b'0', 'Dept', '2019-03-25 09:46:00', 'dept:list', '1'), ('36', b'0', '系统工具', '', '0', '30', 'sys-tools', 'sys-tools', b'0', b'0', null, '2019-03-29 10:57:35', null, '0'), ('37', b'0', '岗位管理', 'system/job/index', '1', '7', 'Steve-Jobs', 'job', b'0', b'0', 'Job', '2019-03-29 13:51:18', 'job:list', '1'), ('38', b'0', '接口文档', 'tools/swagger/index', '36', '36', 'swagger', 'swagger2', b'0', b'0', 'Swagger', '2019-03-29 19:57:53', null, '1'), ('39', b'0', '字典管理', 'system/dict/index', '1', '8', 'dictionary', 'dict', b'0', b'0', 'Dict', '2019-04-10 11:49:04', 'dict:list', '1'), ('41', b'0', '在线用户', 'monitor/online/index', '6', '10', 'Steve-Jobs', 'online', b'0', b'0', 'OnlineUser', '2019-10-26 22:08:43', null, '1'), ('44', b'0', '用户新增', '', '2', '2', '', '', b'0', b'0', '', '2019-10-29 10:59:46', 'user:add', '2'), ('45', b'0', '用户编辑', '', '2', '3', '', '', b'0', b'0', '', '2019-10-29 11:00:08', 'user:edit', '2'), ('46', b'0', '用户删除', '', '2', '4', '', '', b'0', b'0', '', '2019-10-29 11:00:23', 'user:del', '2'), ('48', b'0', '角色创建', '', '3', '2', '', '', b'0', b'0', '', '2019-10-29 12:45:34', 'roles:add', '2'), ('49', b'0', '角色修改', '', '3', '3', '', '', b'0', b'0', '', '2019-10-29 12:46:16', 'roles:edit', '2'), ('50', b'0', '角色删除', '', '3', '4', '', '', b'0', b'0', '', '2019-10-29 12:46:51', 'roles:del', '2'), ('52', b'0', '菜单新增', '', '5', '2', '', '', b'0', b'0', '', '2019-10-29 12:55:07', 'menu:add', '2'), ('53', b'0', '菜单编辑', '', '5', '3', '', '', b'0', b'0', '', '2019-10-29 12:55:40', 'menu:edit', '2'), ('54', b'0', '菜单删除', '', '5', '4', '', '', b'0', b'0', '', '2019-10-29 12:56:00', 'menu:del', '2'), ('56', b'0', '部门新增', '', '35', '2', '', '', b'0', b'0', '', '2019-10-29 12:57:09', 'dept:add', '2'), ('57', b'0', '部门编辑', '', '35', '3', '', '', b'0', b'0', '', '2019-10-29 12:57:27', 'dept:edit', '2'), ('58', b'0', '部门删除', '', '35', '4', '', '', b'0', b'0', '', '2019-10-29 12:57:41', 'dept:del', '2'), ('60', b'0', '岗位新增', '', '37', '2', '', '', b'0', b'0', '', '2019-10-29 12:58:27', 'job:add', '2'), ('61', b'0', '岗位编辑', '', '37', '3', '', '', b'0', b'0', '', '2019-10-29 12:58:45', 'job:edit', '2'), ('62', b'0', '岗位删除', '', '37', '4', '', '', b'0', b'0', '', '2019-10-29 12:59:04', 'job:del', '2'), ('64', b'0', '字典新增', '', '39', '2', '', '', b'0', b'0', '', '2019-10-29 13:00:17', 'dict:add', '2'), ('65', b'0', '字典编辑', '', '39', '3', '', '', b'0', b'0', '', '2019-10-29 13:00:42', 'dict:edit', '2'), ('66', b'0', '字典删除', '', '39', '4', '', '', b'0', b'0', '', '2019-10-29 13:00:59', 'dict:del', '2'), ('116', b'0', '生成预览', 'generator/preview', '36', '999', 'java', 'generator/preview/:tableName', b'1', b'1', 'Preview', '2019-11-26 14:54:36', null, '1'), ('120', null, '11', '1', '0', '999', 'app', '11', b'0', b'0', '1', '2020-04-18 16:24:25', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `data_scope` varchar(255) DEFAULT NULL COMMENT '数据权限',
  `level` int(255) DEFAULT NULL COMMENT '角色级别',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `permission` varchar(255) DEFAULT NULL COMMENT '功能权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色表';

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', '超级管理员', '-', '全部', '1', '2018-11-23 11:04:37', 'admin'), ('2', '普通用户', '-', '本级', '2', '2018-11-23 13:09:06', 'common'), ('3', '临时管理员', '临时管理员', '全部', '3', '2020-04-18 10:26:41', 'temp');
COMMIT;

-- ----------------------------
--  Table structure for `roles_depts`
-- ----------------------------
DROP TABLE IF EXISTS `roles_depts`;
CREATE TABLE `roles_depts` (
  `role_id` bigint(20) NOT NULL,
  `dept_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`dept_id`) USING BTREE,
  KEY `FK7qg6itn5ajdoa9h9o78v9ksur` (`dept_id`) USING BTREE,
  CONSTRAINT `FK7qg6itn5ajdoa9h9o78v9ksur` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`),
  CONSTRAINT `FKrg1ci4cxxfbja0sb0pddju7k` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色部门关联';

-- ----------------------------
--  Table structure for `roles_menus`
-- ----------------------------
DROP TABLE IF EXISTS `roles_menus`;
CREATE TABLE `roles_menus` (
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  KEY `idx_role` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色菜单关联';

-- ----------------------------
--  Records of `roles_menus`
-- ----------------------------
BEGIN;
INSERT INTO `roles_menus` VALUES ('1', '1'), ('2', '1'), ('3', '1'), ('5', '1'), ('35', '1'), ('36', '1'), ('37', '1'), ('38', '1'), ('39', '1'), ('41', '1'), ('44', '1'), ('45', '1'), ('46', '1'), ('48', '1'), ('49', '1'), ('50', '1'), ('52', '1'), ('53', '1'), ('54', '1'), ('56', '1'), ('57', '1'), ('58', '1'), ('60', '1'), ('61', '1'), ('62', '1'), ('64', '1'), ('65', '1'), ('66', '1'), ('116', '1'), ('48', '2'), ('1', '2'), ('49', '2'), ('2', '2'), ('50', '2'), ('3', '2'), ('35', '2'), ('5', '2'), ('37', '2'), ('39', '2'), ('44', '2'), ('1', '3'), ('1', '4');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `enabled` bit(1) DEFAULT NULL COMMENT '状态：1启用、0禁用',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门名称',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `job_id` bigint(20) DEFAULT NULL COMMENT '岗位名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `last_password_reset_time` datetime DEFAULT NULL COMMENT '最后修改密码的日期',
  `nick_name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UK_kpubos9gc2cvtkb0thktkbkes` (`email`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `FK5rwmryny6jthaaxkogownknqp` (`dept_id`) USING BTREE,
  KEY `FKfftoc2abhot8f2wu6cl9a5iky` (`job_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户';

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'https://insbridge-resource.oss-cn-shenzhen.aliyuncs.com/0566da85-010a-4ccb-bcef-647d9629b63e.jpeg', 'iot@qq.com', b'1', '$2a$10$fP.426qKaTmix50Oln8L.uav55gELhAd0Eg66Av4oG86u8km7D/Ky', 'admin', '2', '13344445678', '11', '2018-08-23 09:11:56', '2019-05-18 17:34:21', '管理员', '男'), ('3', null, 'test@qq.com', b'1', '$2a$10$HhxyGZy.ulf3RvAwaHUGb.k.2i9PBpv4YbLMJWp8pES7pPhTyRCF.', 'test', '2', '16688884321', '12', '2018-12-27 20:05:26', '2019-04-01 09:15:24', '临时账户', '男'), ('4', null, '12345@qq.com', b'1', '$2a$10$s8enF/vCM.8SDVP6jlGsnOJ8XaANaAYmwPsL9VBTSHza04X78ityi', '张三', '7', '18812345678', '13', '2020-02-24 21:12:03', null, 'monitor', '男'), ('5', null, 'lisi@qq.com', b'1', '$2a$10$l5Sepq6MpqKUgKGDw6zrJeCYcBAzQje3E8ZpmSgpJNFHdd4DSPYki', '李四', '2', '13309871234', '12', '2020-02-24 21:17:00', null, '李四', '男'), ('6', null, 'wangwu@qq.com', b'1', '$2a$10$XqAGY/k.P77yO8XPJSYPtO0P/YWPaX5x/WXeZi79MpyS3h7tMnRZy', '王五', '7', '17801239876', '13', '2020-02-24 21:18:43', null, '王五', '男');
COMMIT;

-- ----------------------------
--  Table structure for `users_roles`
-- ----------------------------
DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE,
  KEY `FKq4eq273l04bpu4efj0jd0jb98` (`role_id`) USING BTREE,
  CONSTRAINT `FKgd3iendaoyh04b95ykqise6qh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKt4v0rrweyk393bdgt107vdx0x` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户角色关联';

-- ----------------------------
--  Records of `users_roles`
-- ----------------------------
BEGIN;
INSERT INTO `users_roles` VALUES ('1', '1'), ('3', '2'), ('4', '2'), ('5', '3'), ('6', '3');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
