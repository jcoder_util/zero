package com.gitee.zero.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.gitee.zero.commons.utils.MapperUtils;
import com.gitee.zero.commons.utils.OkHttpClientUtils;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * Description: 请求url权限校验
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/3 5:57 下午
 * @see com.gitee.zero.gateway.filter
 */
@Slf4j
//@Component
public class ZeroGatewayGlobalFilter implements GlobalFilter, Ordered {

    private static final String BEARER = "Bearer";

    private static final String URL_OAUTH_CHECK_TOKEN = "http://localhost:9001/oauth/check_token";

    /**
     * 1.首先网关检查token是否有效，无效直接返回401，不调用签权服务
     * 2.调用签权服务器看是否对该请求有权限，有权限进入下一个filter，没有权限返回401
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();
        String authentication = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        String method = request.getMethodValue();
        String url = request.getPath().value();
        log.error("url:{},method:{},headers:{}", url, method, request.getHeaders());
//        String object = oauth2Feign.echo(authentication);
//        log.error("1234"+object);
        //不需要网关签权的url
//        if (authService.ignoreAuthentication(url)) {
//            return chain.filter(exchange);
//        }

        // 通过 HTTP 客户端请求登录接口
        Map<String, String> params = Maps.newHashMap();
        params.put("token", authentication.substring(7));
        System.out.println("123456"+authentication);

        String token = null;
        try {
            // 解析响应结果封装并返回
            Response response = OkHttpClientUtils.getInstance().postData(URL_OAUTH_CHECK_TOKEN, params);
            String jsonString = Objects.requireNonNull(response.body()).string();
            System.out.println("123456"+jsonString);
            Map<String, Object> jsonMap = MapperUtils.json2map(jsonString);
            token = String.valueOf(jsonMap.get("user_name"));
            System.out.println("123456"+token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 如果请求未携带token信息, 直接跳出
        if (StrUtil.isBlank(authentication) || !authentication.startsWith(BEARER)) {
            return chain.filter(exchange);
//            log.debug("url:{},method:{},headers:{}, 请求未携带token信息", url, method, request.getHeaders());
//            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
//            return exchange.getResponse().setComplete();
//            return unauthorized(exchange);
        }


//        //调用签权服务看用户是否有权限，若有权限进入下一个filter
//        if (authService.hasPermission(authentication, url, method)) {
//            ServerHttpRequest.Builder builder = request.mutate();
//            //TODO 转发的请求都加上服务间认证token
//            builder.header(X_CLIENT_TOKEN, "TODO zhoutaoo添加服务间简单认证");
//            //将jwt token中的用户信息传给服务
//            builder.header(X_CLIENT_TOKEN_USER, authService.getJwt(authentication).getClaims());
//            return chain.filter(exchange.mutate().request(builder.build()).build());
//        }
        return chain.filter(exchange);
    }

    /**
     * 网关拒绝，返回401
     *
     * @param
     */
//    @SneakyThrows(ServletException.class)
    private Mono<Void> unauthorized(ServerWebExchange serverWebExchange) {
//        ServerHttpResponse response = serverWebExchange.getResponse();
//        Map map = new HashMap(4);
//        map.put("status", 401);
//        map.put("msg", "没有访问权限");
//        response.setContentType("application/json");
//        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.writeValue(response.getOutputStream(), map);

        serverWebExchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        DataBuffer buffer = serverWebExchange.getResponse()
                .bufferFactory().wrap(HttpStatus.UNAUTHORIZED.getReasonPhrase().getBytes());
        return serverWebExchange.getResponse().writeWith(Flux.just(buffer));
    }


    /**
     * 优先级，值越大优先级越低
     *
     * @return
     */
    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
