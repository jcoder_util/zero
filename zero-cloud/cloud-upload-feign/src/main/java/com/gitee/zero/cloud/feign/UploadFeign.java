package com.gitee.zero.cloud.feign;

import com.gitee.zero.cloud.dto.FileInfo;
import com.gitee.zero.cloud.feign.fallback.UploadFeignFallback;
import com.gitee.zero.commons.dto.ServerResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * Description: 文件上传
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 3:07 下午
 * @see com.gitee.zero.cloud.feign
 */
@Component
@FeignClient(value = "cloud-upload", path = "upload", fallback = UploadFeignFallback.class)
public interface UploadFeign {

    /**
     * 文件上传
     *
     * @param multipartFile {@code MultipartFile}
     * @return {@code String} 文件上传路径
     */
    @PostMapping(value = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ServerResponse<FileInfo> upload(@RequestPart(value = "file") MultipartFile multipartFile);

}

