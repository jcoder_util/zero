package com.gitee.zero.cloud.feign.fallback;

import com.gitee.zero.cloud.dto.FileInfo;
import com.gitee.zero.cloud.feign.UploadFeign;
import com.gitee.zero.commons.dto.ResponseCode;
import com.gitee.zero.commons.dto.ServerResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * Description: 文件上传熔断器
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 3:08 下午
 * @see com.gitee.zero.cloud.feign.fallback
 */
@Component
public class UploadFeignFallback implements UploadFeign {

    private static final String BREAKING_MESSAGE = "请求失败了，请检查您的网络";

    @Override
    public ServerResponse<FileInfo> upload(MultipartFile multipartFile) {
        return ServerResponse.createByErrorCodeMessage(ResponseCode.BREAKING.getCode(), ResponseCode.BREAKING.getDesc());
    }
}
