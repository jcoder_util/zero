package com.gitee.zero.cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * Description: 文件信息
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 3:06 下午
 * @see com.gitee.zero.cloud.dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileInfo implements Serializable {
    /**
     * 文件路径
     */
    private String path;
}

