package com.gitee.zero.cloud;

import com.gitee.zero.commons.security.annotation.EnableZeroResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * <p>
 * Description: 阿里云OSS服务启动类
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:47 下午
 * @see com.gitee.zero.cloud
 */
@SpringCloudApplication
@EnableZeroResourceServer
public class CloudUploadServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(CloudUploadServiceApplication.class, args);
    }
}
