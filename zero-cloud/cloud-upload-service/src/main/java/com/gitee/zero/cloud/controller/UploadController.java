package com.gitee.zero.cloud.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.gitee.zero.cloud.dto.FileInfo;
import com.gitee.zero.commons.dto.ServerResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * <p>
 * Description: 文件上传服务
 * </p>
 *
 * @author 张传臣
 * @version v1.0.0
 * @date 2020/4/20 2:53 下午
 * @see com.gitee.zero.cloud.controller
 */
@RestController
@RequestMapping(value = "upload")
public class UploadController {

    @Value("${base.config.oss.endpoint}")
    private String ENDPOINT;
    @Value("${base.config.oss.access-key-id}")
    private String ACCESS_KEY_ID;
    @Value("${base.config.oss.access-key-secret}")
    private String ACCESS_KEY_SECRET;
    @Value("${base.config.oss.bucket-name}")
    private String BUCKET_NAME;

    private static final String BASE_URL = "https://insbridge-resource.oss-cn-shenzhen.aliyuncs.com/";
    /**
     * 文件上传
     *
     * @param multipartFile @{code MultipartFile}
     * @return {@link ServerResponse<FileInfo>} 文件上传路径
     */
    @PostMapping(value = "")
    public ServerResponse<FileInfo> upload(@RequestParam("file") MultipartFile multipartFile) {
        String fileName = multipartFile.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        String newName = UUID.randomUUID() + "." + suffix;

        OSS client = new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);

        try {
            client.putObject(new PutObjectRequest(BUCKET_NAME, newName, new ByteArrayInputStream(multipartFile.getBytes())));
            // 上传文件路径 = http://BUCKET_NAME.ENDPOINT/自定义路径/fileName
            FileInfo fileInfo = new FileInfo(BASE_URL + newName);
            return ServerResponse.createBySuccess("文件上传成功", fileInfo);
        } catch (IOException e) {
            return ServerResponse.createByErrorMessage("文件上传失败，请重试");
        } finally {
            client.shutdown();
        }
    }
}
